// Package logol provide TODO
package logol

import (
	"os"

	"go.uber.org/zap"
)

// GetLogger returns a logger handler for defined package
//
// Log level is INFO by default, to activate DEBUG, environment variable LOGOL_DEBUG must be set to 1 or true
func GetLogger(packageName string) *zap.Logger {
	//func GetLogger(packageName string) (logger loggo.Logger) {
	var sugar *zap.Logger
	//logger = loggo.GetLogger(packageName)
	osDebug := os.Getenv("LOGOL_DEBUG")
	if osDebug != "" && osDebug != "0" {
		sugar, _ = zap.NewDevelopment()
		//logger.SetLogLevel(loggo.DEBUG)
	} else {
		//logger.SetLogLevel(loggo.INFO)
		config := zap.NewProductionConfig()
		config.OutputPaths = []string{"stdout"}
		sugar, _ = config.Build()
	}
	sugar.With(
		zap.Namespace(packageName),
	)
	//return logger
	return sugar
}
