package logol

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"go.uber.org/zap"

	lru "github.com/hashicorp/golang-lru"
)

// DNA is for DNA sequence
const DNA int = 0

// RNA is for RNA sequence
const RNA int = 1

// PROTEIN is for protein sequence
const PROTEIN int = 2

// FastaInfo describes a fasta sequence in a fasta file (may have multiple sequences)
type FastaInfo struct {
	Sequence string
	Path     string
	Header   string
	Index    int // Index of sequence in file
}

// Sequence describes a sequence to analyze
type Sequence struct {
	Path    string
	Size    int
	Content string
}

// SequenceLru is a LRU cache for sequence parts
type SequenceLru struct {
	Lru      *lru.Cache
	Sequence Sequence
}

// NewSequence initialize a Sequence
func NewSequence(path string) (seq Sequence) {
	seq = Sequence{}
	seq.Path = path
	file, err := os.Open(path)
	if err != nil {
		logger.Error("failed to open sequence", zap.Error(err))
		panic(fmt.Sprintf("%s: %s", "failed to open sequence", err))
	}
	defer file.Close()
	stat, _ := file.Stat()
	seq.Size = int(stat.Size())
	return seq
}

// NewSequenceLru initialize a LRU cache for sequence
func NewSequenceLru(sequence Sequence) (seq SequenceLru) {
	logger.Debug("Initialize sequence LRU")
	seq = SequenceLru{}
	seq.Sequence = sequence
	seq.Lru, _ = lru.New(10)
	return seq
}

// GetContent get content from sequence using LRU cache
func (s SequenceLru) GetContent(start int, end int) (content string) {
	logger.Debug("Search in sequence", zap.Int("start", start), zap.Int("end", end))
	keys := s.Lru.Keys()
	sRange := ""
	sStart := 0
	sEnd := 0
	for _, key := range keys {
		//log.Printf("Cache content: %s", key.(string))
		r := strings.Split(key.(string), ".")
		sStart, _ = strconv.Atoi(r[0])
		sEnd, _ = strconv.Atoi(r[1])
		if start >= sStart && end <= sEnd {
			sRange = key.(string)
			break
		}
	}
	logger.Debug("sequence", zap.Int("start", sStart), zap.Int("end", sEnd))
	if sRange != "" {
		//log.Printf("Load sequence from cache")
		cache, _ := s.Lru.Get(sRange)
		if cache != nil {
			seqPart := cache.(string)
			logger.Debug("cache", zap.Int("start", start), zap.Int("end", end))
			content = seqPart[start-sStart : end-sStart]
			return content
		}
	}
	file, _ := os.Open(s.Sequence.Path)
	defer file.Close()
	if end > s.Sequence.Size {
		end = s.Sequence.Size - 1
	}
	bufferSize := 10000
	if start+bufferSize > s.Sequence.Size {
		bufferSize = end - start
	}
	if end-start > bufferSize {
		bufferSize = end - start
	}
	logger.Debug("Load from sequence", zap.Int("start", start), zap.Int("end", end))
	if bufferSize < 0 {
		return ""
	}
	buffer := make([]byte, bufferSize)
	file.ReadAt(buffer, int64(start))
	// get content
	content = string(buffer)
	key := fmt.Sprintf("%d.%d", start, end)
	//logger.Debugf("Save in LRU %s", key)
	s.Lru.Add(key, content)
	return content

}

// Morphing defines morphing rules
type Morphing map[string]map[string]string

// Morphisms and sequence type
//var morphism map[string]map[string]string

func (m Morphing) initMorphisms(kind string) {
	//morphism := make(map[string]map[string]string)
	complement := make(map[string]string)
	if kind == "" || kind == "dna" || kind == "rna" {
		complement["a"] = "t"
		complement["c"] = "g"
		complement["g"] = "c"
		complement["t"] = "a"
		complement["u"] = "a"
		complement["y"] = "r"
		complement["r"] = "y"
		complement["s"] = "s"
		complement["w"] = "w"
		complement["k"] = "m"
		complement["m"] = "k"
		complement["b"] = "v"
		complement["d"] = "h"
		complement["h"] = "d"
		complement["v"] = "b"
		complement["n"] = "n"
		m["complement"] = complement
	}
}

// AddMorphism adds a new morphism
func (m Morphing) AddMorphism(name string, morph map[string]string) {
	//if morphism == nil {
	//	morphism = make(map[string]map[string]string)
	//	initMorphisms(kind)
	//}
	m[name] = morph
}

// NewMorphisms initialize a new morphing rule map
func NewMorphisms(kind string) Morphing {
	morphisms := make(Morphing)
	morphisms.initMorphisms(kind)
	return morphisms
}

// GetMorphism returns expected morphism rules
func (m Morphing) GetMorphism(kind string, morphName string) (map[string]string, error) {
	//if morphism == nil {
	//	morphism = make(map[string]map[string]string)
	//	initMorphisms(kind)
	//}
	morph, ok := m[morphName]
	if !ok {
		return nil, fmt.Errorf("Morphism does not exists: %s", morphName)
	}
	return morph, nil
}

// BioString represents a DNA/RNA/Protein sequence
type BioString interface {
	GetValue() string
	SetValue(string)
	IsEqual(a rune, b rune) bool
	IsExact(bs BioString) bool
	IsApproximate(bs BioString, subst uint64, indel uint64) (bool, []Match)
	SearchForApproximate(s2 BioString, cost uint64, maxCost uint64, in uint64, del uint64, maxIndel uint64, data1 string, data2 string, prevDel bool, prevAdd bool) []Match
	Reverse() BioString
	Morph(m string, morphisms Morphing, reverse bool) (BioString, error)
}

// NewBioString creates an empty sequence handler according to input type (dna, rna, protein)
func NewBioString(kind int) BioString {
	if kind == DNA || kind == RNA {
		return &DnaString{kind: kind}
	}
	ps := &ProteinString{}
	ps.kind = kind
	return ps
}

// DnaString defines a DNA sequence with optional allowed morphisms
type DnaString struct {
	value string
	kind  int
}

// ProteinString defines a protein sequence
type ProteinString struct {
	DnaString
}

// IsEqual compare to sequence characters
func (s ProteinString) IsEqual(a rune, b rune) bool {
	if a == b {
		return true
	}
	return false
}

// GetValue returns sequence string
func (s DnaString) GetValue() string {
	return s.value
}

// SetValue sets sequence string, lower cased
func (s *DnaString) SetValue(seq string) {
	s.value = strings.ToLower(seq)
}

// IsEqual compare to sequence characters
func (s DnaString) IsEqual(a rune, b rune) bool {
	if a == b {
		return true
	}
	switch a {
	case 'n':
		return true
	case 'y':
		if b == 'c' || b == 't' {
			return true
		}
	case 'r':
		if b == 'a' || b == 'g' {
			return true
		}
	case 's':
		if b == 'g' || b == 'c' {
			return true
		}
	case 'w':
		if b == 'a' || b == 't' {
			return true
		}
	case 'k':
		if b == 't' || b == 'u' || b == 'g' {
			return true
		}
	case 'm':
		if b == 'a' || b == 'c' {
			return true
		}
	case 'b':
		if b != 'a' {
			return true
		}
	case 'd':
		if b != 'c' {
			return true
		}
	case 'h':
		if b != 'g' {
			return true
		}
	case 'v':
		if b == 'a' || b == 'c' || b == 'g' {
			return true
		}
	}

	switch b {
	case 'n':
		return true
	case 'y':
		if a == 'c' || a == 't' {
			return true
		}
	case 'r':
		if a == 'a' || a == 'g' {
			return true
		}
	case 's':
		if a == 'g' || a == 'c' {
			return true
		}
	case 'w':
		if a == 'a' || a == 't' {
			return true
		}
	case 'k':
		if a == 't' || a == 'u' || a == 'g' {
			return true
		}
	case 'm':
		if a == 'a' || a == 'c' {
			return true
		}
	case 'b':
		if a != 'a' {
			return true
		}
	case 'd':
		if a != 'c' {
			return true
		}
	case 'h':
		if a != 'g' {
			return true
		}
	case 'v':
		if a == 'a' || a == 'c' || a == 'g' {
			return true
		}
	}

	return false
}

// IsExact checks sequences are equal
func (s DnaString) IsExact(bs BioString) bool {
	otherSeq := bs.GetValue()
	if len(s.value) != len(otherSeq) {
		return false
	}
	other := []rune(otherSeq)
	for index, val := range s.value {
		if !s.IsEqual(val, other[index]) {
			return false
		}
	}
	return true
}

// IsApproximate compare sequence to an other one, accepting substitutions and/or indel
func (s DnaString) IsApproximate(bs BioString, subst uint64, indel uint64) (bool, []Match) {
	results := s.SearchForApproximate(bs, 0, subst, 0, 0, indel, "", "", false, false)
	for index, r := range results {
		results[index].Len = uint64(len(s.GetValue())) + r.Insert - r.Deletion
		results[index].Pattern = r.Value
	}

	uniques := func(input []Match) []Match {
		u := make([]Match, 0, len(input))
		m := make(map[string]Match)
		for _, val := range input {
			key := fmt.Sprintf("%d-%d-%d", val.Cost, val.Insert, val.Deletion)
			if _, ok := m[key]; !ok {
				m[key] = val
				u = append(u, val)
			}
		}
		return u
	}

	hasResults := len(results) > 0
	// Remove duplicates if indel allowed
	if hasResults && indel > 0 {
		results = uniques(results)
	}

	return hasResults, results
}

// Reverse reverse the sequence value
func (s DnaString) Reverse() BioString {
	r := []rune(s.value)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	rev := NewBioString(s.kind)
	//rev := &DnaString{}
	rev.SetValue(string(r))
	return rev
}

// Morph applies a morphism on sequence value, optionally reversing it
func (s DnaString) Morph(m string, morphisms Morphing, reverse bool) (BioString, error) {
	var morphs map[string]string
	var morphErr error
	if s.kind == PROTEIN {
		morphs, morphErr = morphisms.GetMorphism("protein", m)
	} else {
		morphs, morphErr = morphisms.GetMorphism("dna", m)
	}
	if m == "" && reverse {
		return s.Reverse(), nil
	}
	if morphErr != nil {
		newBioString := NewBioString(s.kind)
		newBioString.SetValue(m)
		return newBioString, morphErr
	}
	morphed := ""
	for i := 0; i < len(s.value); i++ {
		value, ok := morphs[string(s.value[i])]
		if ok {
			morphed += value
		} else {
			morphed += string(s.value[i])
		}
	}

	if reverse {
		rev := []rune(morphed)
		for i, j := 0, len(rev)-1; i < len(rev)/2; i, j = i+1, j-1 {
			rev[i], rev[j] = rev[j], rev[i]
		}

		newBioString := NewBioString(s.kind)
		newBioString.SetValue(string(rev))
		return newBioString, nil
	}

	newBioString := NewBioString(s.kind)
	newBioString.SetValue(morphed)
	return newBioString, nil
}

// SearchForApproximate TODO
func (s DnaString) SearchForApproximate(s2 BioString, cost uint64, maxCost uint64, in uint64, del uint64, maxIndel uint64, data1 string, data2 string, prevDel bool, prevAdd bool) []Match {

	indel := in + del

	logger.Debug("Start IsApproximate cost", zap.Uint64("cost", cost), zap.Uint64("insert", in), zap.Uint64("deletion", del))
	s1Len := uint64(len(s.GetValue()))
	s2Len := uint64(len(s2.GetValue()))
	logger.Debug("Part1", zap.Uint64("len", s1Len), zap.String("value", s.GetValue()))
	logger.Debug("Part2", zap.Uint64("len", s2Len), zap.String("value", s2.GetValue()))

	results := make([]Match, 0)
	if s1Len == 0 && s2Len == 0 {
		logger.Debug("End of comparison, Match!", zap.Uint64("cost", cost), zap.Uint64("insert", in), zap.Uint64("deletion", del))
		m := Match{
			Len:      uint64(s1Len),
			Cost:     cost,
			Distance: in + del,
			Insert:   in,
			Deletion: del,
			Value:    data1 + " vs " + data2,
		}
		results = append(results, m)
		return results
	}
	if s1Len == 0 && s2Len != 0 {
		if prevDel {
			return results
		}
		if indel >= maxIndel {
			logger.Debug("End of comparison, Match!", zap.Uint64("cost", cost), zap.Uint64("insert", in), zap.Uint64("deletion", del))
			m := Match{
				Len:      s1Len,
				Cost:     cost,
				Distance: in + del,
				Insert:   in,
				Deletion: del,
				Value:    data1 + " vs " + data2,
			}
			results = append(results, m)
			return results
		}
		allowedIndels := int(maxIndel - indel)
		if maxIndel-indel >= s2Len {
			allowedIndels = int(s2Len)
		}

		if prevDel {
			return results
		}
		x := ""
		for i := 0; i < allowedIndels; i++ {
			logger.Debug("End of comparison, Match! %d;%d;%d", zap.Uint64("cost", cost), zap.Uint64("insert", in+uint64(i)), zap.Uint64("deletion", del))
			x += "+"
			m := Match{
				Len:      s1Len,
				Cost:     cost,
				Distance: in + uint64(i) + del,
				Insert:   in + uint64(i),
				Deletion: del,
				Value:    data1 + x + " vs " + data2 + s2.GetValue()[0:i],
			}
			results = append(results, m)

		}
		return results

	}
	if s1Len != 0 && s2Len == 0 {
		if prevAdd {
			return results
		}
		if indel >= maxIndel {
			logger.Debug("End of comparison")
			return results
		}
		if maxIndel-indel < s1Len {
			logger.Debug("End of comparison")
			return results
		}
		logger.Debug("End of comparison, Match! %d;%d;%d", zap.Uint64("cost", cost), zap.Uint64("insert", in), zap.Uint64("deletion", del+s1Len))

		x := ""
		y := ""
		for i := 0; i < int(s1Len); i++ {
			x += "-"
			y += " "
		}
		m := Match{
			Len:      s1Len,
			Cost:     cost,
			Distance: in + del + s1Len,
			Insert:   in,
			Deletion: del + s1Len,
			Value:    data1 + x + " vs " + data2 + y,
		}

		results = append(results, m)
		return results

	}

	logger.Debug("Compare", zap.String("s1", string(s.GetValue()[0])), zap.String("s2", string(s2.GetValue()[0])))
	s1Content := s.GetValue()
	s2Content := s2.GetValue()
	s.SetValue(s.GetValue()[0:1])
	s2.SetValue(s2.GetValue()[0:1])
	if !s.IsExact(s2) {
		logger.Debug("Cost?", zap.Uint64("cost", cost), zap.Uint64("maxcost", maxCost))
		if cost < maxCost {
			logger.Debug("Try with cost")
			//news1 := &DnaString{}
			news1 := NewBioString(s.kind)
			news1.SetValue(s1Content[1:])
			news2 := NewBioString(s.kind)
			//news2 := &DnaString{}
			news2.SetValue(s2Content[1:])
			newdata1 := data1 + "X"
			newdata2 := data2 + s2.GetValue()
			tmpRes := news1.SearchForApproximate(news2, cost+1, maxCost, in, del, maxIndel, newdata1, newdata2, false, false)
			results = append(results, tmpRes...)
		}
	} else {
		logger.Debug("Equal, continue...")
		newdata1 := data1 + s.GetValue()
		newdata2 := data2 + s2.GetValue()
		//news1 := &DnaString{}
		news1 := NewBioString(s.kind)
		news1.SetValue(s1Content[1:])
		//news2 := &DnaString{}
		news2 := NewBioString(s.kind)
		news2.SetValue(s2Content[1:])
		tmpRes := news1.SearchForApproximate(news2, cost, maxCost, in, del, maxIndel, newdata1, newdata2, false, false)
		results = append(results, tmpRes...)
	}
	if indel < maxIndel {
		logger.Debug("Try with indel", zap.Uint64("indel", indel), zap.Uint64("max", maxIndel))

		//news1 := &DnaString{}
		news1 := NewBioString(s.kind)
		news1.SetValue(s1Content)
		data11 := data1 + "+"
		data21 := data2 + s2.GetValue()

		//news2 := &DnaString{}
		news2 := NewBioString(s.kind)
		if s2Len > 1 {
			news2.SetValue(s2Content[1:])
		}

		if !prevDel {
			tmpRes := news1.SearchForApproximate(news2, cost, maxCost, in+1, del, maxIndel, data11, data21, false, true)
			results = append(results, tmpRes...)
		}

		//news1bis := &DnaString{}
		news1bis := NewBioString(s.kind)
		if s1Len > 1 {
			news1bis.SetValue(s1Content[1:])
		}
		//news2bis := &DnaString{}
		news2bis := NewBioString(s.kind)
		news2bis.SetValue(s2Content)

		data12 := data1 + "-"
		data22 := data2 + " "

		if !prevAdd {
			tmpRes := news1bis.SearchForApproximate(news2bis, cost, maxCost, in, del+1, maxIndel, data12, data22, true, false)
			results = append(results, tmpRes...)
		}
	}
	logger.Debug("End of comparison")
	return results
}
