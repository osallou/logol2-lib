package logol

import (
	"bytes"
	"fmt"
	"regexp"
	"strings"

	"text/template"

	"go.uber.org/zap"
	yaml "gopkg.in/yaml.v2"
)

// Morphism defines mapping between characters
type Morphism struct {
	Morph map[string]string `yaml:"morph"`
}

// ParamDef defines function inputs and outputs
type ParamDef struct {
	Comment string
	Inputs  []string
	Outputs []string
}

// VariableModel TODO
type VariableModel struct {
	Name    string
	Comment string
	Param   []string
	Module  bool // Is a module call
}

// Repeat TODO
type Repeat struct {
	Min     string
	Max     string
	Spacer  RangeConstraint
	Overlap RangeConstraint
}

// RangeConstraint TODO
type RangeConstraint struct {
	Min string
	Max string
}

// StringConstraint TODO
type StringConstraint struct {
	Comment  string
	Content  string
	SaveAs   string `yaml:"saveas"`
	Size     RangeConstraint
	Start    RangeConstraint
	End      RangeConstraint
	Morphism string `yaml:"morphism"` // name of morphism
	Reverse  bool
}

// StructConstraint TODO
type StructConstraint struct {
	Comment  string
	Cost     RangeConstraint // range [3,4]
	Distance RangeConstraint // range [10,20]
}

// NegConstraint TODO
type NegConstraint struct {
	Value             string
	StringConstraints StringConstraint
	StructConstraints StructConstraint
}

// PluginConstraint is a search or check constraint
type PluginConstraint struct {
	Search Plugin   `yaml:"search"` // Plugin to use for search
	Checks []Plugin `yaml:"checks"` // List of plugin controls
}

// Variable TODO
type Variable struct {
	Comment           string
	Value             string
	Next              []string
	Model             VariableModel
	StringConstraints StringConstraint `yaml:"string_constraints"`
	StructConstraints StructConstraint `yaml:"struct_constraints"`
	// NegativeConstraints []NegConstraint
	Overlap           bool
	OverlapConstraint RangeConstraint
	Not               bool
	Spacer            bool
	SpacerConstraint  RangeConstraint `yaml:"spacer_constraints"`
	Repeat            Repeat
	Plugin            PluginConstraint
}

// Model TODO
type Model struct {
	Comment string
	Param   []string
	Meta    []string
	Start   []string
	Vars    map[string]Variable
}

// ModelDef TODO
type ModelDef struct {
	Model   string
	Param   []string
	Nomatch string // Id of the variable the model should not match
	Comment string
}

// PluginDef defines a remote Go plugin
type PluginDef struct {
	Name    string // Name of the plugin
	Package string // Go package URL
}

// Plugin defines name and params for a plugin
type Plugin struct {
	Name   string // Name of the plugin (must match a PluginDef)
	Params string // Comma separated list of parameters
}

// Grammar defines the grammar used in search
type Grammar struct {
	Comment   string
	Options   map[string]int64
	Morphisms map[string]Morphism `yaml:"morphisms"`
	Models    map[string]Model
	Run       []ModelDef
	Meta      []string // Meta constraints on models e.g. vars.mod1.Len < vars.mod2.Len
	Version   string   // Optional version of grammar
	Plugins   []PluginDef
}

// getUsedVars looks for var calls vars.XXX. in input compute string
func getUsedVars(compute string) []string {
	usedVars := make([]string, 0)
	re := regexp.MustCompile(`vars\.(\w+).`)
	matches := re.FindAllSubmatch([]byte(compute), -1)
	for _, submatch := range matches {
		usedVars = append(usedVars, string(submatch[1]))
	}
	return usedVars
}

// Check verifies that a variable is fine
func (v Variable) Check(grammar Grammar, modelName string, varName string) (bool, []string) {
	usedVars := make([]string, 0)
	isOk := true
	for _, next := range v.Next {
		if _, ok := grammar.Models[modelName].Vars[next]; !ok {
			fmt.Printf("%s.%s: next is %s but is not defined\n", modelName, varName, next)
			isOk = false
		}
	}
	if v.Overlap && v.OverlapConstraint.Min != "" && v.OverlapConstraint.Max == "" {
		fmt.Printf("%s.%s: Overlap minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}

	if v.Spacer && v.SpacerConstraint.Min != "" && v.SpacerConstraint.Max == "" {
		fmt.Printf("%s.%s: Spacer minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}
	if v.Repeat.Min != "" && v.Repeat.Max == "" {
		fmt.Printf("%s.%s: Repeat minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}
	if v.Repeat.Spacer.Min != "" && v.Repeat.Spacer.Max == "" {
		fmt.Printf("%s.%s: Repeat spacer minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}
	if v.Repeat.Overlap.Min != "" && v.Repeat.Overlap.Max == "" {
		fmt.Printf("%s.%s: Repeat overlap minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}
	if v.Model.Name != "" {
		isPlugin := false
		for _, plugin := range grammar.Plugins {
			if strings.HasPrefix(v.Model.Name, plugin.Name) {
				isPlugin = true
			}
		}
		if !isPlugin {
			m, ok := grammar.Models[v.Model.Name]
			if !ok {
				fmt.Printf("%s.%s: calls model %s but is not defined\n", modelName, varName, v.Model.Name)
				isOk = false
			} else {
				if len(v.Model.Param) != len(m.Param) {
					fmt.Printf("%s.%s: calls model %s but number fo parameters are different\n", modelName, varName, v.Model.Name)
					isOk = false
				}
			}
		}
	}
	if v.Plugin.Search.Name != "" {
		found := false
		for _, p := range grammar.Plugins {
			if v.Plugin.Search.Name == p.Name {
				found = true
				break
			}
		}
		if !found {
			fmt.Printf("%s.%s: use of plugin %s but not defined", modelName, varName, v.Plugin.Search)
			isOk = false
		}
	}
	for _, check := range v.Plugin.Checks {
		found := false
		for _, p := range grammar.Plugins {
			if check.Name == p.Name {
				found = true
				break
			}
		}
		if !found {
			fmt.Printf("%s.%s: use of plugin %s but not defined", modelName, varName, check.Name)
			isOk = false
		}
	}
	if v.StringConstraints.Content != "" {
		usedVars = append(usedVars, v.StringConstraints.Content)
	}
	if v.StringConstraints.Morphism != "" && v.StringConstraints.Morphism != "complement" {
		if _, ok := grammar.Morphisms[v.StringConstraints.Morphism]; !ok {
			fmt.Printf("%s.%s: calls morphism %s but is not defined", modelName, varName, v.StringConstraints.Morphism)
			isOk = false
		}
	}
	if v.StructConstraints.Cost.Min != "" && v.StructConstraints.Cost.Max == "" {
		fmt.Printf("%s.%s: cost minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}
	if v.StructConstraints.Distance.Min != "" && v.StructConstraints.Distance.Max == "" {
		fmt.Printf("%s.%s: distance minimum set, but no maximum\n", modelName, varName)
		isOk = false
	}
	if v.Not && v.StringConstraints.Size.Max == "" {
		fmt.Printf("%s.%s: *not* constraint set but no size constraint set, but no maximum\n", modelName, varName)
		isOk = false
	}

	if v.OverlapConstraint.Min != "" {
		used := getUsedVars(v.OverlapConstraint.Min)
		usedVars = append(usedVars, used...)
	}
	if v.OverlapConstraint.Max != "" {
		used := getUsedVars(v.OverlapConstraint.Max)
		usedVars = append(usedVars, used...)
	}
	if v.SpacerConstraint.Min != "" {
		used := getUsedVars(v.SpacerConstraint.Min)
		usedVars = append(usedVars, used...)
	}
	if v.SpacerConstraint.Max != "" {
		used := getUsedVars(v.SpacerConstraint.Max)
		usedVars = append(usedVars, used...)
	}
	if v.Repeat.Min != "" {
		used := getUsedVars(v.Repeat.Min)
		usedVars = append(usedVars, used...)
	}
	if v.Repeat.Max != "" {
		used := getUsedVars(v.Repeat.Max)
		usedVars = append(usedVars, used...)
	}
	if v.Repeat.Spacer.Min != "" {
		used := getUsedVars(v.Repeat.Spacer.Min)
		usedVars = append(usedVars, used...)
	}
	if v.Repeat.Spacer.Max != "" {
		used := getUsedVars(v.Repeat.Spacer.Max)
		usedVars = append(usedVars, used...)
	}
	if v.Repeat.Overlap.Min != "" {
		used := getUsedVars(v.Repeat.Overlap.Min)
		usedVars = append(usedVars, used...)
	}
	if v.Repeat.Overlap.Max != "" {
		used := getUsedVars(v.Repeat.Overlap.Max)
		usedVars = append(usedVars, used...)
	}
	if v.StructConstraints.Cost.Min != "" {
		used := getUsedVars(v.StructConstraints.Cost.Min)
		usedVars = append(usedVars, used...)
	}
	if v.StructConstraints.Cost.Max != "" {
		used := getUsedVars(v.StructConstraints.Cost.Max)
		usedVars = append(usedVars, used...)
	}
	if v.StructConstraints.Distance.Min != "" {
		used := getUsedVars(v.StructConstraints.Distance.Min)
		usedVars = append(usedVars, used...)
	}
	if v.StructConstraints.Distance.Max != "" {
		used := getUsedVars(v.StructConstraints.Distance.Max)
		usedVars = append(usedVars, used...)
	}

	if v.StringConstraints.Size.Min != "" {
		used := getUsedVars(v.StringConstraints.Size.Min)
		usedVars = append(usedVars, used...)
	}
	if v.StringConstraints.Size.Max != "" {
		used := getUsedVars(v.StringConstraints.Size.Max)
		usedVars = append(usedVars, used...)
	}
	if v.StringConstraints.Start.Min != "" {
		used := getUsedVars(v.StringConstraints.Start.Min)
		usedVars = append(usedVars, used...)
	}
	if v.StringConstraints.Start.Max != "" {
		used := getUsedVars(v.StringConstraints.Start.Max)
		usedVars = append(usedVars, used...)
	}

	if v.StringConstraints.End.Min != "" {
		used := getUsedVars(v.StringConstraints.End.Min)
		usedVars = append(usedVars, used...)
	}
	if v.StringConstraints.End.Max != "" {
		used := getUsedVars(v.StringConstraints.End.Max)
		usedVars = append(usedVars, used...)
	}
	return isOk, usedVars
}

// Check verifies that a model is fine
func (m Model) Check(grammar Grammar, modelName string) bool {
	isOk := true
	if len(m.Start) == 0 {
		fmt.Printf("%s: No start defined for model\n", modelName)
		isOk = false
	}
	params := make(map[string]bool)
	for _, p := range m.Param {
		params[p] = true
	}
	usedVars := make([]string, 0)
	for varName, variable := range m.Vars {
		if variable.StringConstraints.SaveAs != "" {
			params[variable.StringConstraints.SaveAs] = true
		}
		varOk, varUses := variable.Check(grammar, modelName, varName)
		if !varOk {
			fmt.Printf("%s: Invalid variable %s\n", modelName, varName)
		}
		for _, varUse := range varUses {
			usedVars = append(usedVars, varUse)
		}
		if len(variable.Model.Param) > 0 {
			for _, param := range variable.Model.Param {
				params[param] = true
			}
		}
	}
	for _, usedVar := range usedVars {
		if _, ok := params[usedVar]; !ok {
			fmt.Printf("%s: Var %s used but not a param nor a saved variable", modelName, usedVar)
			isOk = false
		}
	}
	return isOk
}

// Check verifies that grammar is fine
func (g Grammar) Check() bool {
	isOk := true
	for modelName, model := range g.Models {
		if !model.Check(g, modelName) {
			isOk = false
			fmt.Printf("Invalid model %s\n", modelName)
		}
	}
	return isOk

}

// LoadGrammar loads a grammar file content
func LoadGrammar(grammar []byte) (Grammar, error) {
	g := Grammar{}
	err := yaml.Unmarshal(grammar, &g)
	return g, err
}

// ModelInfo TODO
type ModelInfo struct {
	Name   string
	Params []string
	Code   string
	Desc   string
}

func getNextCalls(modelName string, varName string, model Model, varNames []string, isModel bool, isMain bool, plugins *PluginConstraint) string {

	var tmpl = `
	{{ $nbChans := len .VarNames}}
	{{ $modelName := .ModelName }}
	{{ $modelVarName := .VarName }}
	{{ $isMain := .IsMain }}
	{{ $plugins := .Plugins}}
{{range $index, $varName := .VarNames}}
	chin_{{$modelName}}_{{$varName}} := make(chan logol.Match)
	chout_{{$modelName}}_{{$varName}} := make(chan logol.Match)
	// call {{$modelName}}_{{$varName}}
	go {{$modelName}}_{{$varName}}(chin_{{$modelName}}_{{$varName}}, chout_{{$modelName}}_{{$varName}}, ctx)
{{end}}
	
	// Should search for pattern and send matches to chin_x_x
	nbChan := {{$nbChans}} + 1
	nbChanStop := 0
	/*
{{ if gt $nbChans 0 }}
	nbChan := {{$nbChans}}
	nbChanStop := 0
{{ end }}
	*/
	noMoreMatches := false
	{{if ne $modelName $modelVarName}}
	repeat := false
	{{end}}
	// Chans for repeat, will be used only if we have some repeats enabled
	chin_repeat := make(chan logol.Match)
	chout_repeat := make(chan logol.Match)
	chout_repeat_closed := false

	go func() {
		for true {
			msgIn, more := <-chin
			// TODO if main model and modelName=modelVarName, set spacer to true
			logger.Debug("Message received", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"), zap.Any("msg", msgIn), zap.Bool("more", more))
			{{if $isMain}}
			msgIn.Vars = logol.SwitchCtxVars("{{$modelName}}", ctx, msgIn.Vars, msgIn.PrevVars, false)
			{{end}}
			
			
			{{if eq $modelName $modelVarName}}
			{{ if $isMain }}
				msgIn.Spacer = true
			{{ end }}
			{{ end }}
			if more {
				if msgIn.RepeatIndex > 0 {
					ctx.IsRepeat = true
				}
				ctx.Progress <- "{{$modelName}}_{{$modelVarName}}:T"
				ctx.Position = msgIn.Position
				ctx.Vars = make(map[string]logol.Match)
				if msgIn.Spacer {
					ctx.Spacer = true
				} else {
					ctx.Spacer = false
				}
				for k,v := range msgIn.Vars {
					ctx.Vars[k] = v
				}
				{{ if $isMain }}
				/*
				if msgIn.PrevVars != nil {
					for k, v := range msgIn.PrevVars {
						if _, ok := ctx.Vars[k]; !ok {
							ctx.Vars[k] = v
						}
						if _, ok := msgIn.Vars[k]; !ok {
							msgIn.Vars[k] = v
						}
					}
				}
				*/
				{{ end }}
	{{if eq $modelName $modelVarName}}
		{{range $index, $aChan := .VarNames }}
				logger.Debug("send msg", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$aChan}}"), zap.Any("msg", msgIn))
				chin_{{$modelName}}_{{$aChan}} <- msgIn
		{{end}}
		if !chout_repeat_closed {
			close(chout_repeat)
			chout_repeat_closed = true
			nbChanStop++
		}
	{{else}}
				var prevMatch logol.Match
				//prevMatches := logol.CloneMatches(msgIn.PrevFoundMatch)
				prevMatches := msgIn.PrevFoundMatch

				
				hasPrevMatch := false
				if len(msgIn.PrevFoundMatch) > 0 {
					prevMatch = msgIn.PrevFoundMatch[0]
					prevMatches = prevMatches[1:]
					hasPrevMatch = true
				} else {
					if prevMatches == nil {
						prevMatches = make([]logol.Match, 0)
					}
				}
				{{ if $plugins.Search.Name }}
				fc := {{$plugins.Search.Name}}.NewSearchComponent(ctx, "{{$modelName}}", "{{$modelVarName}}", hasPrevMatch, prevMatch, {{$plugins.Search.Params}})
				{{ else }}
				
				fc := logol.NewSearchComponent(ctx, "{{$modelName}}", "{{$modelVarName}}", hasPrevMatch, prevMatch, prevMatches)
				{{end}}
				go fc.Items()
				nbMatches := 0
				for m := range fc.Iterator() {
					if os.Getenv("LOGOL_SLOW") != "" {
						slow, errSlow := strconv.Atoi(os.Getenv("LOGOL_SLOW"))
						if errSlow == nil {
						time.Sleep(time.Duration(slow) * time.Second)
						}
					}
					if m.Defined {
						ctx.Progress <- "{{$modelName}}_{{$modelVarName}}:M"
					}
					if m.Pattern == "" {
						m.Pattern = "{{$modelName}}-{{$modelVarName}}"
					}
					m.RepeatIndex = msgIn.RepeatIndex

					{{ range $plugin := $plugins.Checks }}
					if !{{$plugin.Name}}.PostControl(ctx, m, "{{$plugin.Params}}") {
						logger.Debug("Plugin postcontrol failed, skipping", zap.String("plugin", "{{$plugin.Name}}"))
						continue
					}
					{{ end }}

					//prevFound := fc.GetPrevMatches()
					tmpMatches := make([]logol.Match, len(msgIn.Match))
					copy(tmpMatches, msgIn.Match)
					
					newMsg := logol.Match{
						Position: m.Position + m.Len,
						Match:    append(tmpMatches, m),
						Model: "{{$modelName}}",
						VarName: "{{$modelVarName}}",
						PrevFoundMatch: prevMatches,
						PrevVars: m.PrevVars,
					}
					

					newMsg.Vars = make(map[string]logol.Match)
					// for k, v := range fc.Context().Vars {
					for k, v := range msgIn.Vars {
						newMsg.Vars[k] = v
					}
					for k, v := range m.Vars {
						newMsg.Vars[k] = v
					}

					saveAs, save := fc.SaveAs()
					if save {
						logger.Debug("Save", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"), zap.String("as", saveAs), zap.Any("match",m))
						newMsg.Vars[saveAs] = m
						if m.RepeatIndex > 0 && m.Defined {
							newMsg.Vars[saveAs] = logol.MergeRepeats(ctx, m, msgIn.Match[len(msgIn.Match) - int(m.RepeatIndex):])
						}
					}
					nbMatches++

					// Manage repeat case, if not yet started
					// Do we have repeats?
					methRepeatMinName := "{{$modelName}}_{{$modelVarName}}RepeatMin"
					methRepeatMaxName := "{{$modelName}}_{{$modelVarName}}RepeatMax"
					minRepeat, minRepeatErr := logol.CallComputeFunction(methRepeatMinName, ctx)
					maxRepeat, maxRepeatErr := logol.CallComputeFunction(methRepeatMaxName, ctx)
					if minRepeatErr != nil || maxRepeatErr != nil {
						newMsg.Defined = false
					} else {
						if minRepeat > 0 && maxRepeat == 0 {
							panic("MaxRepeat must be > 0")
						}
						if (minRepeat == 0 && maxRepeat == 0) || m.RepeatIndex >= uint64(maxRepeat) - 1{
							if !chout_repeat_closed {
								close(chout_repeat)
								chout_repeat_closed = true
								nbChanStop++
							}
						} else {
							if m.Defined {
								if !repeat {
									repeat = true
									// for repeat call ourselves {{$modelName}}_{{$modelVarName}}
									logger.Debug("repeat, call myself", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"))
									go {{$modelName}}_{{$modelVarName}}(chin_repeat, chout_repeat, ctx)
								}
								// Send back to myself
								repeatMsg := newMsg
								repeatMsg.RepeatIndex = msgIn.RepeatIndex + 1
								if !hasPrevMatch || (prevMatches[0].Model == "{{$modelName}}" && prevMatches[0].VarName == "{{$modelVarName}}") {
									logger.Debug("send to chin_repeat", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"), zap.Any("msg", newMsg)) 
									chin_repeat <- repeatMsg
								}
								// If min not reached or max reached, skip other next components
								if newMsg.RepeatIndex < uint64(minRepeat) || newMsg.RepeatIndex > uint64(maxRepeat) {
									continue
								}
							}
						}
					}


		{{if eq $nbChans 0}}
					chout <- newMsg
		{{else}}
					if !m.Defined {
						newMsg.Spacer = true
					}
		{{end}}

		{{range $index, $aChan := .VarNames }}
					if !hasPrevMatch || (prevMatches[0].Model == "{{$modelName}}" && prevMatches[0].VarName == "{{$aChan}}") {
						logger.Debug("send", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$aChan}}"), zap.Any("msg", newMsg))
						chin_{{$modelName}}_{{$aChan}} <- newMsg
					}
		{{end}}
				} // End of fc.Iterator
	{{end}}
			} else {
				logger.Debug("Receive stop request", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"))
				{{if eq $nbChans 0}}
					if ! repeat {
					logger.Debug("send stop message back to out channel", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"))
					close(chout)
					noMoreMatches = true
					} else {
						close(chin_repeat)
					}
				{{else}}
					{{range $index, $aChan := .VarNames}}
					logger.Debug("send stop", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$aChan}}"))
					close(chin_{{$modelName}}_{{$aChan}})
					{{end}}
					close(chin_repeat)
				{{end}}
				{{if eq $modelName $modelVarName}}
				repeat := false
				{{end}}
					if !repeat && !chout_repeat_closed {
						close(chout_repeat)
						chout_repeat_closed = true
						nbChanStop++
					}
					break
			}			
			
		}
	}()
{{range $index, $aChan := .VarNames}}
	chout_{{$modelName}}_{{$aChan}}_closed := false
{{end}}
	for ! noMoreMatches {

		select {
{{range $index, $aChan := .VarNames}}
			case msgOut, more := <-chout_{{$modelName}}_{{$aChan}}:
				if more {
					logger.Debug("got msg from chout", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$aChan}}"))
					{{if eq $modelName $modelVarName}}
					isOk, isErr := logol.ModelMetaControls(ctx, "{{$modelName}}", msgOut)
					if isErr != nil {
						logger.Debug("match does not defined expected vars, accepting anyway", zap.String("model", "{{$modelName}}"))

					} else {
						if !isOk {
							continue
						}
					}
					{{end}}
					{{if $isMain}}
					msgOut.PrevVars = msgOut.Vars
					msgOut.Vars = logol.SwitchCtxVars("{{$modelName}}", ctx, msgOut.Vars, msgOut.PrevVars, true)
					{{end}}
					chout <- msgOut
				} else {
					if ! chout_{{$modelName}}_{{$aChan}}_closed {
						nbChanStop++
						logger.Debug("got close from", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$aChan}}"))
						if nbChanStop == nbChan {
							noMoreMatches = true
							close(chout)
						}
						chout_{{$modelName}}_{{$aChan}}_closed = true
					}
				}
{{end}}
			case msgOut, more := <-chout_repeat:
				if more {
					logger.Debug("got msg from chout_repeat", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"))
					{{if $isMain}}
					msgOut.PrevVars = msgOut.Vars
					msgOut.Vars = logol.SwitchCtxVars("{{$modelName}}", ctx, msgOut.Vars, msgOut.PrevVars, true)
					{{end}}
					chout <- msgOut
				} else {
					if ! chout_repeat_closed {
						nbChanStop++
						logger.Debug("got close from chout_repeat", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"))
						if nbChanStop == nbChan {
							noMoreMatches = true
							close(chout)
						}
						chout_repeat_closed = true
					}
				}
		}
	}
/*
{{if gt $nbChans 0 }}
		}
{{end}}
*/
{{if eq $nbChans 0}}
	for ! noMoreMatches {
		time.Sleep(1 * time.Second)
	}
{{end}}
	logger.Debug("Exit", zap.String("model", "{{$modelName}}"), zap.String("var", "{{$modelVarName}}"))
`
	type Data struct {
		ModelName string
		VarName   string
		VarNames  []string
		Plugins   *PluginConstraint
		IsMain    bool
	}
	tp, err := template.New("getNextCalls").Parse(tmpl)
	if err != nil {
		panic(err)
	}
	data := Data{
		ModelName: modelName,
		VarName:   varName,
		VarNames:  varNames,
		Plugins:   plugins,
		IsMain:    isMain,
	}
	var code bytes.Buffer
	err = tp.Execute(&code, data)
	if err != nil {
		panic(err)
	}

	return code.String()
}

// GenerateModel TODO
func GenerateModel(name string, model Model, isMain bool) (string, error) {
	data := `
// {{.Name}} var called by {{.Desc}}
func {{.Name}}(chin chan logol.Match, chout chan logol.Match, ctx logol.Context) error {
	logger.Debug("Start model", zap.String("model", "{{.Name}}"))
	{{.Code}}
	return nil
}
`
	varDef := ""
	modelCode := ""
	var writer bytes.Buffer

	evalFunc := `
{{range $key, $code := .Code}}
func {{$key}}(ctx logol.Context) (int64, error) {
	{{$code}}
}
{{end}}
`

	type evalFuncStruct struct {
		Name string
		Code map[string]string
	}
	tmpl, err := template.New("evalFun").Parse(evalFunc)
	evalData := evalFuncStruct{
		Name: name,
		Code: make(map[string]string),
	}
	for variableName, variableDef := range model.Vars {
		evalData.Code[name+"_"+variableName+"SizeMin"] = EvalComputeFunction(variableDef.StringConstraints.Size.Min)
		evalData.Code[name+"_"+variableName+"SizeMax"] = EvalComputeFunction(variableDef.StringConstraints.Size.Max)

		evalData.Code[name+"_"+variableName+"StartMin"] = EvalComputeFunction(variableDef.StringConstraints.Start.Min)
		evalData.Code[name+"_"+variableName+"StartMax"] = EvalComputeFunction(variableDef.StringConstraints.Start.Max)

		evalData.Code[name+"_"+variableName+"SpacerMin"] = EvalComputeFunction(variableDef.SpacerConstraint.Min)
		evalData.Code[name+"_"+variableName+"SpacerMax"] = EvalComputeFunction(variableDef.SpacerConstraint.Max)

		evalData.Code[name+"_"+variableName+"EndMin"] = EvalComputeFunction(variableDef.StringConstraints.End.Min)
		evalData.Code[name+"_"+variableName+"EndMax"] = EvalComputeFunction(variableDef.StringConstraints.End.Max)

		evalData.Code[name+"_"+variableName+"CostMin"] = EvalComputeFunction(variableDef.StructConstraints.Cost.Min)
		evalData.Code[name+"_"+variableName+"CostMax"] = EvalComputeFunction(variableDef.StructConstraints.Cost.Max)

		evalData.Code[name+"_"+variableName+"DistanceMin"] = EvalComputeFunction(variableDef.StructConstraints.Distance.Min)
		evalData.Code[name+"_"+variableName+"DistanceMax"] = EvalComputeFunction(variableDef.StructConstraints.Distance.Max)

		evalData.Code[name+"_"+variableName+"RepeatMin"] = EvalComputeFunction(variableDef.Repeat.Min)
		evalData.Code[name+"_"+variableName+"RepeatMax"] = EvalComputeFunction(variableDef.Repeat.Max)
		evalData.Code[name+"_"+variableName+"RepeatSpacerMin"] = EvalComputeFunction(variableDef.Repeat.Spacer.Min)
		evalData.Code[name+"_"+variableName+"RepeatSpacerMax"] = EvalComputeFunction(variableDef.Repeat.Spacer.Max)
		evalData.Code[name+"_"+variableName+"RepeatOverlapMin"] = EvalComputeFunction(variableDef.Repeat.Overlap.Min)
		evalData.Code[name+"_"+variableName+"RepeatOverlapMax"] = EvalComputeFunction(variableDef.Repeat.Overlap.Max)

		evalData.Code[name+"_"+variableName+"OverlapMin"] = EvalComputeFunction(variableDef.OverlapConstraint.Min)
		evalData.Code[name+"_"+variableName+"OverlapMax"] = EvalComputeFunction(variableDef.OverlapConstraint.Max)

	}

	err = tmpl.Execute(&writer, evalData)
	if err != nil {
		logger.Error("Error during template parsing", zap.Error(err))
	}
	varDef += fmt.Sprint(writer.String())
	writer.Reset()

	for varName, varValue := range model.Vars {
		modelInfo := ModelInfo{
			Name:   fmt.Sprintf("%s_%s", name, varName),
			Params: make([]string, 0),
			Code:   getNextCalls(name, varName, model, varValue.Next, false, false, &varValue.Plugin), // Should set a call for each next var
			Desc:   model.Comment,
		}
		tmpl, err := template.New("model").Parse(data)
		if err != nil {
			return "", err
		}
		err = tmpl.Execute(&writer, modelInfo)
		varDef += fmt.Sprint(writer.String())
		writer.Reset()
	}
	for _, start := range model.Start {
		modelCode += fmt.Sprintf("// call %s_%s\n", name, start)
	}
	modelDef := ""
	/*
		params := make([]string, len(model.Param))
		for i, param := range model.Param {
			params[i] = param
		}
	*/
	modelInfo := ModelInfo{
		Name:   name,
		Params: model.Param,
		Code:   getNextCalls(name, name, model, model.Start, true, isMain, nil),
	}

	tmpl, err = template.New("model").Parse(data)
	if err != nil {
		return "", err
	}
	err = tmpl.Execute(&writer, modelInfo)
	modelDef += fmt.Sprint(writer.String())
	writer.Reset()

	return modelDef + varDef, nil
}

// ModelCall TODO
type ModelCall struct {
	Fn     string
	Params []string
}

// Context TODO
type Context struct {
	Position uint64
	Spacer   bool
	// Match       *Match
	Vars          map[string]Match
	Progress      chan string
	Grammar       *Grammar
	SequenceInput string // path to input sequence
	Sequence      *Sequence
	SequenceLru   *SequenceLru
	Kind          string // DNA or RNA or Protein, defaults DNA
	IsRepeat      bool   // Are we searching for repeats
	CassieHandler *Cassie
	Models        map[string]interface{} // Map of functions to call models and variables
	Morphisms     Morphing               // List of available morphisms
}

// DoResult TODO
func DoResult(match Match) {
	logger.Error("Not yet implemented")
	logger.Info("Match", zap.Any("match", match))
}

// AndModels TODO
func AndModels(models []ModelCall) (string, string) {
	data := ""
	inputChan := "chin"
	outputChan := "chout"
	for _, model := range models {
		outputChan = model.Fn + "Chan"
		data += fmt.Sprintf(`
		%s := make(chan logol.Match)	
		go %s(%s, %s, ctx)
		`, outputChan, model.Fn, inputChan, outputChan)
		inputChan = outputChan
	}
	// data += "chout := " + outputChan + "\n"
	return data, outputChan
}
