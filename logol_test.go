package logol

import (
	"strings"
	"testing"
)

func fakeFn(ctx Context) (int64, error) {
	return 0, nil
}

func expectMatch(matches []Match, position uint64, len uint64) (bool, Match) {
	found := false
	match := Match{}
	for _, m := range matches {
		if m.Position == position && m.Len == len {
			match = m
			found = true
			break
		}
	}
	return found, match
}

func newContext(grammar Grammar, position uint64, spacer bool) Context {
	seq := NewSequence("testdata/sequence.txt")
	lru := NewSequenceLru(seq)
	ctx := Context{
		Position: position,
		Spacer:   spacer,
		// Match: nil,
		Progress:      make(chan string),
		Grammar:       &grammar,
		SequenceInput: "testdata/sequence.txt",
		Sequence:      &seq,
		SequenceLru:   &lru,
		Models:        make(map[string]interface{}),
	}
	ctx.CassieHandler = NewCassieManager("testdata/sequence.txt", 0)
	return ctx
}

func setUp(ctx Context, modelName, varName string) {
	//Models = make(map[string]interface{})
	setModel(ctx, modelName, varName)
}

func setModel(ctx Context, modelName, varName string) {
	funcs := []string{"SizeMin", "SizeMax", "StartMin", "StartMax", "EndMin", "EndMax", "SpacerMin", "SpacerMax", "CostMin", "CostMax", "DistanceMin", "DistanceMax", "RepeatMin", "RepeatMax", "RepeatSpacerMin", "RepeatSpacerMax", "RepeatOverlapMin", "RepeatOverlapMax", "OverlapMin", "OverlapMax"}
	for _, fn := range funcs {
		if _, ok := ctx.Models[modelName+"_"+varName+fn]; !ok {
			ctx.Models[modelName+"_"+varName+fn] = fakeFn
		}
	}
}

func getMatchesWithPrev(ctx Context, modelName string, varName string, hasPrevMatch bool, prevMatch Match, prevMatches []Match) []Match {
	matches := make([]Match, 0)
	fc := NewSearchComponent(ctx, modelName, varName, hasPrevMatch, prevMatch, prevMatches)

	go fc.Items()
	for m := range fc.Iterator() {
		matches = append(matches, m)
	}
	return matches
}

func getMatches(ctx Context, modelName string, varName string) []Match {
	matches := getMatchesWithPrev(ctx, modelName, varName, false, Match{}, make([]Match, 0))
	return matches
}

func TestGrammarExact1(t *testing.T) {
	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		Value: "cccc",
	}
	ctx := newContext(grammar, 0, false)
	setUp(ctx, "mod1", "var1")
	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if found, _ := expectMatch(matches, 0, 4); !found {
		t.Errorf("should have a match at pos 0, len 4, got %+v", matches[0])
	}
}

func TestGrammarExact2(t *testing.T) {

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		Spacer: false,
		Value:  "aaaaaa",
	}

	ctx := newContext(grammar, 0, true)
	setUp(ctx, "mod1", "var1")
	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if found, _ := expectMatch(matches, 4, 6); !found {
		t.Errorf("should have a match at pos 4, len 6, got %+v", matches[0])
	}
}

func TestGrammarSizeRange1(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 2, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 4, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		StringConstraints: StringConstraint{
			Size: RangeConstraint{Min: "2", Max: "4"},
		},
	}

	ctx := newContext(grammar, 4, false)
	setUp(ctx, "mod1", "var1")
	ctx.Models["mod1_var1SizeMin"] = minFn
	ctx.Models["mod1_var1SizeMax"] = maxFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if found, _ := expectMatch(matches, 4, 2); !found {
		t.Errorf("should have a match at pos 4, len 2, got %+v", matches)
	}
	if found, _ := expectMatch(matches, 4, 2); !found {
		t.Errorf("should have a match at pos 4, len 3, got %+v", matches)
	}
	if found, _ := expectMatch(matches, 4, 2); !found {
		t.Errorf("should have a match at pos 4, len 4, got %+v", matches)
	}
}

func TestGrammarSizeRange2(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 2, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 4, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		StringConstraints: StringConstraint{
			Size: RangeConstraint{Min: "2", Max: "4"},
		},
	}

	ctx := newContext(grammar, 9, true)
	setUp(ctx, "mod1", "var1")
	ctx.Models["mod1_var1SizeMin"] = minFn
	ctx.Models["mod1_var1SizeMax"] = maxFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if len(matches) != 1 || matches[0].Defined == true {
		t.Errorf("match should not be defined, got %v", matches)
		return
	}

	setModel(ctx, "mod1", "var2")
	minFn2 := func(ctx Context) (int64, error) {
		return 0, nil
	}
	maxFn2 := func(ctx Context) (int64, error) {
		return 0, nil
	}
	ctx.Models["mod1_var2SpacerMin"] = minFn2
	ctx.Models["mod1_var2SpacerMax"] = maxFn2
	prevMatches := make([]Match, 1)
	prevMatches[0] = Match{
		Model:    "mod1",
		VarName:  "var2",
		Defined:  true,
		Position: 13,
		Len:      4,
	}
	ctx.Position = 0
	ctx.Spacer = true
	matches2 := getMatchesWithPrev(ctx, "mod1", "var1", true, matches[0], prevMatches)
	if len(matches2) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}

	if found, _ := expectMatch(matches2, 8, 2); found {
		t.Errorf("should not have a match at pos 8, len 2, got %+v", matches2)
	}
	if found, _ := expectMatch(matches2, 9, 2); !found {
		t.Errorf("should have a match at pos 9, len 2, got %+v", matches2)
	}
	if found, _ := expectMatch(matches2, 9, 2); !found {
		t.Errorf("should have a match at pos 9, len 3, got %+v", matches2)
	}
	if found, _ := expectMatch(matches2, 9, 2); !found {
		t.Errorf("should have a match at pos 9, len 4, got %+v", matches2)
	}

	if found, _ := expectMatch(matches2, 12, 2); !found {
		t.Errorf("should have a match at pos 12, len 2, got %+v", matches2)
	}
	if found, _ := expectMatch(matches2, 12, 3); !found {
		t.Errorf("should have a match at pos 12, len 2, got %+v", matches2)
	}
	if found, _ := expectMatch(matches2, 12, 4); !found {
		t.Errorf("should have a match at pos 12, len 2, got %+v", matches2)
	}

	setModel(ctx, "mod1", "var3")
	minFn3 := func(ctx Context) (int64, error) {
		return 0, nil
	}
	maxFn3 := func(ctx Context) (int64, error) {
		return 2, nil
	}
	ctx.Models["mod1_var3SpacerMin"] = minFn3
	ctx.Models["mod1_var3SpacerMax"] = maxFn3
	prevMatches = make([]Match, 1)
	prevMatches[0] = Match{
		Model:    "mod1",
		VarName:  "var3",
		Defined:  true,
		Position: 13,
		Len:      4,
	}
	ctx.Position = 0
	ctx.Spacer = true
	matches3 := getMatchesWithPrev(ctx, "mod1", "var1", true, matches[0], prevMatches)
	if len(matches3) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}

	if found, _ := expectMatch(matches3, 6, 2); found {
		t.Errorf("should not have a match at pos 6, len 2, got %+v", matches3)
	}
	if found, _ := expectMatch(matches3, 7, 2); !found {
		t.Errorf("should have a match at pos 7, len 2, got %+v", matches3)
	}
	if found, _ := expectMatch(matches3, 7, 2); !found {
		t.Errorf("should have a match at pos 9, len 3, got %+v", matches3)
	}
	if found, _ := expectMatch(matches3, 7, 2); !found {
		t.Errorf("should have a match at pos 9, len 4, got %+v", matches3)
	}

	if found, _ := expectMatch(matches3, 12, 2); !found {
		t.Errorf("should have a match at pos 12, len 2, got %+v", matches3)
	}
	if found, _ := expectMatch(matches3, 12, 3); !found {
		t.Errorf("should have a match at pos 12, len 2, got %+v", matches3)
	}
	if found, _ := expectMatch(matches3, 12, 4); !found {
		t.Errorf("should have a match at pos 12, len 2, got %+v", matches3)
	}

}

func TestEvalComputeFunction(t *testing.T) {
	test := ""
	testRes := "return 0, nil"
	res := EvalComputeFunction(test)
	if res != testRes {
		t.Errorf("should have %s, got %s", testRes, res)
	}
	test = "vars.R1.Len"
	res = EvalComputeFunction(test)
	if !strings.Contains(res, "ctx.Vars[\"R1\"]") {
		t.Errorf("code should refer to context var: got %s", res)
	}

	test = "vars.R1.Len + vars.R2.Len"
	res = EvalComputeFunction(test)
	if !strings.Contains(res, "ctx.Vars[\"R1\"]") || !strings.Contains(res, "ctx.Vars[\"R2\"]") {
		t.Errorf("code should refer to context var: got %s", res)
	}

}

func TestGrammarError1(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 1, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 1, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		Value: "tttgtt",
		StructConstraints: StructConstraint{
			Cost: RangeConstraint{Min: "1", Max: "1"},
		},
	}

	ctx := newContext(grammar, 0, true)
	setUp(ctx, "mod1", "var1")
	ctx.Models["mod1_var1CostMin"] = minFn
	ctx.Models["mod1_var1CostMax"] = maxFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	found, match := expectMatch(matches, 12, 6)
	if !found {
		t.Errorf("should have a match at pos 12, len 6, got %+v", matches[0])
		return
	}
	if match.Cost != 1 {
		t.Errorf("should have a cost of 1")
	}
}

func TestGrammarError2(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 1, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 2, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		Value: "tttgtg",
		StructConstraints: StructConstraint{
			Cost: RangeConstraint{Min: "1", Max: "2"},
		},
	}

	ctx := newContext(grammar, 0, true)
	setModel(ctx, "mod1", "var1")
	ctx.Models["mod1_var1CostMin"] = minFn
	ctx.Models["mod1_var1CostMax"] = maxFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	found, match := expectMatch(matches, 12, 6)
	if !found {
		t.Errorf("should have a match at pos 12, len 6, got %+v", matches[0])
		return
	}
	if match.Cost != 2 {
		t.Errorf("should have a cost of 2")
	}
}

func TestGrammarOverlap(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 0, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 3, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		Value:   "aaaaaa",
		Overlap: true,
		OverlapConstraint: RangeConstraint{
			Min: "0",
			Max: "3",
		},
	}

	ctx := newContext(grammar, 5, false)
	setModel(ctx, "mod1", "var1")
	ctx.Models["mod1_var1OverlapMin"] = minFn
	ctx.Models["mod1_var1OverlapMax"] = maxFn
	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if found, _ := expectMatch(matches, 4, 6); !found {
		t.Errorf("should have a match at pos 4, len 6, got %+v", matches[0])
	}
}

func TestGrammarNot1(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 1, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 3, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		StringConstraints: StringConstraint{
			Size: RangeConstraint{Min: "1", Max: "3"},
		},
		Not:   true,
		Value: "cc",
	}

	ctx := newContext(grammar, 0, false)
	setModel(ctx, "mod1", "var1")
	ctx.Models["mod1_var1SizeMin"] = minFn
	ctx.Models["mod1_var1SizeMax"] = maxFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if found, _ := expectMatch(matches, 0, 1); !found {
		t.Errorf("should have a match at pos 0, len 1, got %+v", matches[0])
	}
	if found, _ := expectMatch(matches, 0, 3); !found {
		t.Errorf("should have a match at pos 0, len 1, got %+v", matches[1])
	}
}

func TestGrammarNot2(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 4, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 4, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		StringConstraints: StringConstraint{
			Size: RangeConstraint{Min: "4", Max: "4"},
		},
		Not:   true,
		Value: "acgt",
	}

	ctx := newContext(grammar, 7, true)
	setModel(ctx, "mod1", "var1")
	ctx.Models["mod1_var1SizeMin"] = minFn
	ctx.Models["mod1_var1SizeMax"] = maxFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}

	if found, _ := expectMatch(matches, 8, 4); !found {
		t.Errorf("should have a match at pos 8, got %+v", matches)
	}
	if found, _ := expectMatch(matches, 10, 4); !found {
		t.Errorf("should have a match at pos 10, got %+v", matches)
	}
	if found, m := expectMatch(matches, 9, 4); found {
		t.Errorf("should not have a match at pos 9, got %+v", m)
	}
}

func TestGrammarNot3(t *testing.T) {
	minFn := func(ctx Context) (int64, error) {
		return 1, nil
	}
	maxFn := func(ctx Context) (int64, error) {
		return 3, nil
	}

	minCostFn := func(ctx Context) (int64, error) {
		return 1, nil
	}
	maxCostFn := func(ctx Context) (int64, error) {
		return 1, nil
	}

	grammar := Grammar{
		Models: make(map[string]Model),
	}
	grammar.Models["mod1"] = Model{
		Vars: make(map[string]Variable),
	}
	grammar.Models["mod1"].Vars["var1"] = Variable{
		StringConstraints: StringConstraint{
			Size: RangeConstraint{Min: "1", Max: "3"},
		},
		Not:   true,
		Value: "ca",
		StructConstraints: StructConstraint{
			Cost: RangeConstraint{Min: "1", Max: "1"},
		},
	}

	ctx := newContext(grammar, 0, false)
	setModel(ctx, "mod1", "var1")
	ctx.Models["mod1_var1SizeMin"] = minFn
	ctx.Models["mod1_var1SizeMax"] = maxFn
	ctx.Models["mod1_var1CostMin"] = minCostFn
	ctx.Models["mod1_var1CostMax"] = maxCostFn

	matches := getMatches(ctx, "mod1", "var1")
	if len(matches) == 0 {
		t.Errorf("should have a match, got no match")
		return
	}
	if found, _ := expectMatch(matches, 0, 1); !found {
		t.Errorf("should have a match at pos 0, len 1, got %+v", matches[0])
	}
	if found, _ := expectMatch(matches, 0, 3); !found {
		t.Errorf("should have a match at pos 0, len 1, got %+v", matches[1])
	}
}
