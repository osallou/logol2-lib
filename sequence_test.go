package logol

import (
	"testing"
)

func TestApproximate(t *testing.T) {
	b1 := &DnaString{}
	b1.SetValue("acgt")
	b2 := &DnaString{}
	b2.SetValue("actt")
	matches := b1.SearchForApproximate(b2, 0, 0, 0, 0, 0, "", "", false, false)
	if len(matches) > 0 {
		t.Errorf("should not be ok, got matches %+v", matches)
	}
}

func TestApproximateCost(t *testing.T) {
	b1 := &DnaString{}
	b1.SetValue("acgt")
	b2 := &DnaString{}
	b2.SetValue("actt")
	matches := b1.SearchForApproximate(b2, 0, 1, 0, 0, 0, "", "", false, false)
	if len(matches) == 0 {
		t.Errorf("should have matches")
	}

	b2 = &DnaString{}
	b2.SetValue("acga")
	matches = b1.SearchForApproximate(b2, 0, 1, 0, 0, 0, "", "", false, false)
	if len(matches) == 0 {
		t.Errorf("should have matches")
	}
}

func TestApproximateDistance(t *testing.T) {
	b1 := &DnaString{}
	b1.SetValue("acgt")
	b2 := &DnaString{}
	b2.SetValue("aacgt")
	matches := b1.SearchForApproximate(b2, 0, 0, 0, 0, 1, "", "", false, false)
	if len(matches) == 0 {
		t.Errorf("should have matches")
	}

	b2 = &DnaString{}
	b2.SetValue("aacgt")
	matches = b1.SearchForApproximate(b2, 0, 0, 0, 0, 3, "", "", false, false)
	if len(matches) == 0 {
		t.Errorf("should have matches")
	}

	b1 = &DnaString{}
	b1.SetValue("acgt")
	b2 = &DnaString{}
	b2.SetValue("aacgt")
	matches = b2.SearchForApproximate(b1, 0, 0, 0, 0, 3, "", "", false, false)
	if len(matches) == 0 {
		t.Errorf("should have matches")
	}
	//js, _ := json.Marshal(matches)
	//fmt.Printf("? %+s\n", string(js))

}
func TestError(t *testing.T) {
	b1 := &DnaString{}
	b1.SetValue("acgt")
	morphs := NewMorphisms("dna")

	_, err := b1.Morph("err", morphs, false)
	if err == nil {
		t.Errorf("should raise an error")
	}

}

func TestReverse(t *testing.T) {
	b1 := &DnaString{}
	b1.SetValue("acgt")
	b2 := b1.Reverse()
	if b2.GetValue() != "tgca" {
		t.Errorf("should have tgca, got %s", b2.GetValue())
	}

	morphs := NewMorphisms("dna")

	b2, _ = b1.Morph("", morphs, true)
	if b2.GetValue() != "tgca" {
		t.Errorf("should have tgca, got %s", b2.GetValue())
	}
}

func TestEqualSequence(t *testing.T) {
	//b1 := &DnaString{}
	b1 := NewBioString(DNA)
	b1.SetValue("acgt")
	//b2 := &DnaString{}
	b2 := NewBioString(DNA)
	b2.SetValue("acgt")
	//b3 := &DnaString{}
	b3 := NewBioString(DNA)
	b3.SetValue("tgca")

	if !b1.IsExact(b2) {
		t.Errorf("should be equal %s <==> %s", b1.GetValue(), b2.GetValue())
	}
	if b1.IsExact(b3) {
		t.Errorf("should be different %s <==> %s", b1.GetValue(), b2.GetValue())
	}
	morphs := NewMorphisms("dna")

	// complement
	b1compl, _ := b1.Morph("complement", morphs, false)
	if !b1compl.IsExact(b3) {
		t.Errorf("should be equal %s <==> %s", b1compl.GetValue(), b3.GetValue())
	}

	// reverse complement
	b1complrev, _ := b1.Morph("complement", morphs, true)
	if !b1complrev.IsExact(b1) {
		t.Errorf("should be equal %s <==> %s", b1compl.GetValue(), b3.GetValue())
	}
}
