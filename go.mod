module gitlab.inria.fr/osallou/logol2-lib

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/hashicorp/golang-lru v0.5.3
	gitlab.inria.fr/osallou/suffixsearch v0.3.2
	go.uber.org/zap v1.13.0
	gopkg.in/yaml.v2 v2.2.7
)
