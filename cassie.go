package logol

import (
	"sync"

	cassiopee "gitlab.inria.fr/osallou/suffixsearch"

	"go.uber.org/zap"
)

// var onceCassie sync.Once
var logger = GetLogger("logol.cassie")

// CassieSearchOptions sets search options
type CassieSearchOptions struct {
	Mode      int
	MaxSubst  int
	MaxIndel  int
	Ambiguity bool
}

// Cassie object to index and search in indexed sequence
type Cassie struct {
	Sequence string
	Kind     int
	//Indexer  *cassie.CassieIndexer
	//Searcher *cassie.CassieSearch
	Indexer *cassiopee.Root
	PLen    int
	mux     sync.Mutex
}

// NewCassieManager returns a new Cassie instance
func NewCassieManager(sequence string, kind int) *Cassie {
	return &Cassie{
		Sequence: sequence,
		Kind:     kind,
	}
}

// IsCassieProteinEqual equality function for proteins
func IsCassieProteinEqual(a byte, b byte) bool {
	if a == b {
		return true
	}
	return false
}

// IsCassieDnaEqual equality function for dna/rna
func IsCassieDnaEqual(a byte, b byte) bool {
	if a == b {
		return true
	}
	switch a {
	case 'n':
		return true
	case 'y':
		if b == 'c' || b == 't' {
			return true
		}
	case 'r':
		if b == 'a' || b == 'g' {
			return true
		}
	case 's':
		if b == 'g' || b == 'c' {
			return true
		}
	case 'w':
		if b == 'a' || b == 't' {
			return true
		}
	case 'k':
		if b == 't' || b == 'u' || b == 'g' {
			return true
		}
	case 'm':
		if b == 'a' || b == 'c' {
			return true
		}
	case 'b':
		if b != 'a' {
			return true
		}
	case 'd':
		if b != 'c' {
			return true
		}
	case 'h':
		if b != 'g' {
			return true
		}
	case 'v':
		if b == 'a' || b == 'c' || b == 'g' {
			return true
		}
	}

	switch b {
	case 'n':
		return true
	case 'y':
		if a == 'c' || a == 't' {
			return true
		}
	case 'r':
		if a == 'a' || a == 'g' {
			return true
		}
	case 's':
		if a == 'g' || a == 'c' {
			return true
		}
	case 'w':
		if a == 'a' || a == 't' {
			return true
		}
	case 'k':
		if a == 't' || a == 'u' || a == 'g' {
			return true
		}
	case 'm':
		if a == 'a' || a == 'c' {
			return true
		}
	case 'b':
		if a != 'a' {
			return true
		}
	case 'd':
		if a != 'c' {
			return true
		}
	case 'h':
		if a != 'g' {
			return true
		}
	case 'v':
		if a == 'a' || a == 'c' || a == 'g' {
			return true
		}
	}

	return false
}

// Close closes resources
func (c *Cassie) Close() {
	c.Indexer.Close()
	c.Indexer = nil
}

// Index indexes sequence, once and only once
func (c *Cassie) Index(ctx Context) {
	c.mux.Lock()
	defer c.mux.Unlock()
	//onceCassie.Do(func() {
	if c.Indexer == nil {
		options := cassiopee.Options{
			Compressed: true,
		}
		if ctx.Grammar.Options != nil && ctx.Grammar.Options["MAX_PATTERN_LENGTH"] != 0 {
			options.MaxPatternLen = ctx.Grammar.Options["MAX_PATTERN_LENGTH"]
		}
		if ctx.Kind == "protein" {
			c.Indexer = cassiopee.NewRoot(c.Sequence, IsCassieProteinEqual, options)
		} else {
			c.Indexer = cassiopee.NewRoot(c.Sequence, IsCassieDnaEqual, options)
		}
		logger.Debug("Indexing sequence", zap.String("sequence", c.Sequence))
		c.Indexer.Index()
		logger.Debug("Indexing done", zap.String("sequence", c.Sequence))
	}
}

// Search parse index to find matches
func (c *Cassie) Search(ctx Context, ch chan Match, value string, maxCost int, maxDist int, model string, varName string) {

	mch := make(chan cassiopee.Match)
	go c.Indexer.Search(mch, value, int64(maxCost), int64(maxDist))
	for m := range mch {
		logger.Debug("Match ", zap.Any("match", m), zap.String("value", value))
		pLen := uint64(len(value))
		if m.Insert-m.Deletion != 0 {
			pLen = pLen + uint64(m.Insert-m.Deletion)
		}
		match := Match{
			Defined:  true,
			Position: uint64(m.Position),
			Len:      uint64(pLen),
			Cost:     uint64(m.Subst),
			Distance: uint64(m.Insert + m.Deletion),
			Spacer:   true,
		}
		match.Value = ctx.SequenceLru.GetContent(int(match.Position), int(match.Position+match.Len))
		ch <- match
	}
	close(ch)
}
