package logol

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strings"

	"go.uber.org/zap"
)

// Models defines mapping between function name and real function
// var Models map[string]interface{}

// Result is the structure of a result match
type Result struct {
	Fasta   FastaInfo
	Matches []Match
}

// SearchComponent is base interface for all search components
type SearchComponent interface {
	Search() (*Match, error)
	Iterator() (cm chan Match)
	Items()
	GetComponent() Variable
	SaveAs() (string, bool)
	Context() Context
	//GetPrevMatches() []Match
}

// FindExactComponent defines search components
type FindExactComponent struct {
	Cm                 chan Match
	Component          Variable
	Ctx                Context
	Position           uint64
	ModelName          string
	VarName            string
	PrevMatch          Match
	HasPrevMatch       bool
	PrevFollowingMatch Match
	//PrevMatches        []Match
}

// FindCassieComponent calls cassiopee suffix tree search for matches with spacer
type FindCassieComponent struct {
	FindExactComponent
}

// Items TODO
func (fc FindCassieComponent) Items() {
	logger.Debug("FindCassieComponent:Items", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	if fc.HasPrevMatch && fc.PrevMatch.Defined {
		logger.Debug("FindCassieComponent: Already found, skipping", zap.Any("match", fc.PrevMatch))
		if fc.Check(fc.PrevMatch) {
			m := fc.PrevMatch
			(&m).fixPosition(fc.Ctx.Position)
			fc.Cm <- m
		}
		close(fc.Cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		logger.Debug("Constraints failed", zap.Error(checkErr))
		match := Match{
			Model:    fc.ModelName,
			VarName:  fc.VarName,
			Position: fc.Ctx.Position,
			Pattern:  fc.Component.Comment,
			Defined:  false,
		}
		fc.Cm <- match
		close(fc.Cm)
		return
	}

	constraint := fc.Component.Value

	if fc.Component.StringConstraints.Content != "" {
		ctxVar, ok := fc.Ctx.Vars[fc.Component.StringConstraints.Content]
		if !ok {
			match := Match{
				Model:    fc.ModelName,
				VarName:  fc.VarName,
				Position: fc.Ctx.Position,
				Len:      0,
				Pattern:  fc.Component.Comment,
				Defined:  false,
			}
			fc.Cm <- match
			close(fc.Cm)
			return
		}
		constraint = fc.Ctx.SequenceLru.GetContent(int(ctxVar.Position), int(ctxVar.Position+ctxVar.Len))
	}

	if constraint == "" {
		close(fc.Cm)
		return
	}

	originalPosition := fc.Ctx.Position

	seq1 := &DnaString{}
	seq1.SetValue(constraint)
	if fc.Component.StringConstraints.Morphism != "" {
		morphseq1, morphErr := seq1.Morph(fc.Component.StringConstraints.Morphism, fc.Ctx.Morphisms, fc.Component.StringConstraints.Reverse)
		if morphErr != nil {
			logger.Error("Morph error", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Error(morphErr))
			close(fc.Cm)
		}
		constraint = morphseq1.GetValue()
	}

	methMaxDistanceName := fc.ModelName + "_" + fc.VarName + "DistanceMax"
	maxDistance, _ := CallComputeFunction(methMaxDistanceName, fc.Ctx)

	methMaxCostName := fc.ModelName + "_" + fc.VarName + "CostMax"
	maxCost, _ := CallComputeFunction(methMaxCostName, fc.Ctx)

	handler := fc.Ctx.CassieHandler
	handler.Index(fc.Ctx)
	cassieChan := make(chan Match)

	foundMatches := make(map[string]bool)

	go handler.Search(fc.Ctx, cassieChan, constraint, int(maxCost), int(maxDistance), fc.ModelName, fc.VarName)
	for match := range cassieChan {
		match.SpacerLen = match.Position - fc.Ctx.Position
		if fc.Ctx.Position > match.Position {
			match.SpacerLen = 0
		}
		if match.Position < fc.Ctx.Position {
			match.OverlapLen = fc.Ctx.Position - match.Position
		}
		checked, checkErr := fc.CheckConstraints(match, true)
		if checkErr == nil && checked {
			match.Model = fc.ModelName
			match.VarName = fc.VarName
			if fc.Component.Not {
				foundMatches[fmt.Sprintf("%d-%d", match.Position, match.Len)] = true
			} else {
				fc.Cm <- match
			}
		}
	}

	// Manage not condition
	if fc.Component.Not {
		fc.FindNotMatches(originalPosition, foundMatches, fc.Cm)
	}

	close(fc.Cm)
}

// FindApproximateComponent search for string with errors
type FindApproximateComponent struct {
	FindExactComponent
}

// Items  TODO
func (fc FindApproximateComponent) Items() {
	logger.Debug("FindApproximateComponent:Items", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	if fc.HasPrevMatch && fc.PrevMatch.Defined {
		logger.Debug("FindApproximateComponent: Already found, skipping", zap.Any("match", fc.PrevMatch))
		if fc.Check(fc.PrevMatch) {
			m := fc.PrevMatch
			(&m).fixPosition(fc.Ctx.Position)
			fc.Cm <- m
		}
		close(fc.Cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		logger.Debug("Constraints failed", zap.Error(checkErr))
		match := Match{
			Model:    fc.ModelName,
			VarName:  fc.VarName,
			Position: fc.Ctx.Position,
			Pattern:  fc.Component.Comment,
			Defined:  false,
		}
		fc.Cm <- match
		close(fc.Cm)
		return
	}

	constraint := fc.Component.Value

	if fc.Component.StringConstraints.Content != "" {
		ctxVar, ok := fc.Ctx.Vars[fc.Component.StringConstraints.Content]
		if !ok {
			match := Match{
				Model:    fc.ModelName,
				VarName:  fc.VarName,
				Position: fc.Ctx.Position,
				// TODO could check if relates to a constant string or size constrained var to set min size
				Len:     0,
				Pattern: fc.Component.Comment,
				Defined: true,
			}
			fc.Cm <- match
			close(fc.Cm)
			return
		}
		constraint = fc.Ctx.SequenceLru.GetContent(int(ctxVar.Position), int(ctxVar.Position+ctxVar.Len))
	}

	if constraint == "" {
		close(fc.Cm)
		return
	}

	var minSpacer int64
	var maxSpacer int64
	var maxOverlap int64

	foundMatches := make(map[string]bool)
	originalPosition := fc.Ctx.Position
	// Global spacer
	if fc.Ctx.Spacer {
		maxSpacer = int64(fc.Ctx.Sequence.Size) - int64(fc.Ctx.Position)
	} else {
		if fc.Ctx.IsRepeat {
			methSpacerMinName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMin"
			methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.Ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.Ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
			methOverlapMaxName := fc.ModelName + "_" + fc.VarName + "RepeatOverlapMax"
			maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.Ctx)
			fc.Ctx.Position = originalPosition - uint64(maxOverlap)
		} else {
			methSpacerMinName := fc.ModelName + "_" + fc.VarName + "SpacerMin"
			methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "SpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.Ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.Ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
		}
	}

	methStartMinName := fc.ModelName + "_" + fc.VarName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.Ctx)
	if minStart > 0 {
		fc.Ctx.Position = uint64(minStart)
	}

	if minSpacer > 0 {
		fc.Ctx.Position += uint64(minSpacer)
	}

	methMaxDistanceName := fc.ModelName + "_" + fc.VarName + "DistanceMax"
	maxDistance, _ := CallComputeFunction(methMaxDistanceName, fc.Ctx)

	methMaxCostName := fc.ModelName + "_" + fc.VarName + "CostMax"
	maxCost, _ := CallComputeFunction(methMaxCostName, fc.Ctx)

	logger.Debug("FindApproximateComponent:Spacer", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Int64("spacer", maxSpacer), zap.Any("ctx", fc.Ctx))
	for i := 0; i <= int(maxOverlap+maxSpacer); i++ {

		content := fc.Ctx.SequenceLru.GetContent(int(fc.Ctx.Position), int(fc.Ctx.Position)+len(constraint)+int(maxDistance))
		if content == "" {
			logger.Debug("FindApproximateComponent:no match found", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
			close(fc.Cm)
			return
		}
		logger.Debug("FindApproximateComponent:Compare", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("constraint", constraint), zap.String("content", content))

		seq1 := &DnaString{}
		seq1.SetValue(constraint)
		seq2 := &DnaString{}
		seq2.SetValue(content)
		var matches []Match
		hasMatches := false
		if fc.Component.StringConstraints.Morphism != "" {
			morphseq1, morphErr := seq1.Morph(fc.Component.StringConstraints.Morphism, fc.Ctx.Morphisms, fc.Component.StringConstraints.Reverse)
			if morphErr != nil {
				logger.Error("Morph error", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Error(morphErr))
				close(fc.Cm)
				return
			}
			hasMatches, matches = morphseq1.IsApproximate(seq2, uint64(maxCost), uint64(maxDistance))
		} else {
			hasMatches, matches = seq1.IsApproximate(seq2, uint64(maxCost), uint64(maxDistance))
		}
		logger.Debug("FindApproximate results", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Any("matches", matches))
		if hasMatches {
			for _, m := range matches {
				match := Match{
					Model:          fc.ModelName,
					VarName:        fc.VarName,
					Value:          content[0:m.Len],
					Position:       fc.Ctx.Position,
					Len:            m.Len,
					Cost:           m.Cost,
					Distance:       m.Distance,
					Vars:           fc.Ctx.Vars,
					PrevFoundMatch: CloneMatches(fc.PrevMatch.PrevFoundMatch),
					Defined:        true,
					Pattern:        m.Pattern,
				}

				match.SpacerLen = match.Position - originalPosition
				if originalPosition > match.Position {
					match.SpacerLen = 0
				}
				if match.Position < originalPosition {
					match.OverlapLen = originalPosition - match.Position
				}
				if fc.Component.Not {
					foundMatches[fmt.Sprintf("%d-%d", match.Position, match.Len)] = true
				} else {
					checked, checkErr := fc.CheckConstraints(match, true)
					if checkErr == nil && checked {
						fc.Cm <- match
					}
				}
			}
		}

		fc.Ctx.Position++
	}

	if fc.Component.Not {
		fc.FindNotMatches(originalPosition, foundMatches, fc.Cm)
	}

	close(fc.Cm)
}

// FindModelComponent calls an other model
type FindModelComponent struct {
	FindExactComponent
}

// Items TODO
func (fc FindModelComponent) Items() {
	logger.Debug("FindModelComponent", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	var calledModelParams []string
	calledModel := fc.Ctx.Grammar.Models[fc.Component.Model.Name]

	if fc.Component.Model.Module {
		fnParam, ok := fc.Ctx.Models[fc.Component.Model.Name+"Params"]
		if !ok {
			logger.Error("Model not defined", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("call", fc.Component.Model.Name+"Params"))
			close(fc.Cm)
			return
		}
		fnp := reflect.ValueOf(fnParam)
		in := make([]reflect.Value, 0)
		values := fnp.Call(in)
		calledModelParams = values[0].Interface().([]string)
		logger.Debug("module call", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Any("params", calledModelParams))
	} else {
		calledModelParams = calledModel.Param
	}
	calledModelName := fc.Component.Model.Name
	fn, ok := fc.Ctx.Models[calledModelName]
	if !ok {
		logger.Error("Model not defined", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("call", calledModelName))
		close(fc.Cm)
		return
	}
	//logger.Debug("call model ", zap.String("model", calledModelName))
	chin := make(chan Match)
	chout := make(chan Match)
	newCtx := fc.Ctx
	newCtx.Vars = make(map[string]Match)

	originalPosition := fc.Ctx.Position

	methStartMinName := fc.ModelName + "_" + fc.VarName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.Ctx)
	if minStart > 0 {
		fc.Ctx.Position = uint64(minStart)
	}

	matchStart := Match{
		Model:    fc.ModelName,
		VarName:  fc.VarName,
		Value:    "init",
		Position: fc.Ctx.Position,
		Match:    make([]Match, 0),
		Vars:     make(map[string]Match),
	}

	/* OSALLOU
	if fc.HasPrevMatch && fc.PrevMatch.PrevVars != nil {
		for k, v := range fc.PrevMatch.PrevVars {
			newCtx.Vars[k] = v
			matchStart.Vars[k] = v
		}
	} */
	var prevVars map[string]Match
	if fc.HasPrevMatch && fc.PrevMatch.PrevVars != nil {
		prevVars = fc.PrevMatch.PrevVars
	}

	if fc.HasPrevMatch {
		matchStart.PrevFoundMatch = CloneMatches(fc.PrevMatch.PrevFoundMatch)
	}

	if fc.Ctx.Spacer {
		matchStart.Spacer = true
	}
	if fc.Component.Spacer {
		matchStart.Spacer = true
	}

	newVars := SwitchCtxFromToVars(fc.ModelName, fc.Component.Model.Param, calledModelParams, fc.Ctx.Vars, prevVars, false)
	matchStart.Vars = newVars
	newCtx.Vars = newVars

	fnc := reflect.ValueOf(fn)
	in := make([]reflect.Value, 3)
	in[0] = reflect.ValueOf(chin)
	in[1] = reflect.ValueOf(chout)
	in[2] = reflect.ValueOf(newCtx)
	logger.Debug("Call model with context", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("call", calledModelName), zap.Any("ctx", newCtx))
	go fnc.Call(in)
	chin <- matchStart
	close(chin)

	foundMatches := make(map[string]bool)
	for msg := range chout {
		logger.Debug("Got result message from model", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("call", calledModelName), zap.Any("msg", msg))
		match := MergeMatches(msg, fc.Context(), fc.Component)

		match.Model = fc.ModelName
		match.VarName = fc.VarName
		// update vars with received parameters
		match.Vars = SwitchCtxFromToVars(fc.ModelName, fc.Component.Model.Param, calledModelParams, msg.Vars, prevVars, true)
		match.PrevVars = make(map[string]Match)
		for k, v := range msg.Vars {
			match.PrevVars[k] = v
		}
		logger.Debug("Updated message from model", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("call", calledModelName), zap.Any("match", match))

		checked, checkErr := fc.CheckConstraints(match, true)

		if checkErr == nil && checked {
			if fc.Component.Not {
				foundMatches[fmt.Sprintf("%d-%d", match.Position, match.Len)] = true
			} else {
				fc.Cm <- match

			}
		}
	}

	if fc.Component.Not {
		fc.FindNotMatches(originalPosition, foundMatches, fc.Cm)
	}

	logger.Debug("Exiting from model call", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("call", calledModelName))
	close(fc.Cm)
}

// FindSizeRangeComponent defines component to get a string between min and max size
type FindSizeRangeComponent struct {
	FindExactComponent
	count int64
	min   int64
	max   int64
}

// Items loop over possible matches and send matches to iterator chan
func (fc FindSizeRangeComponent) Items() {
	logger.Debug("FindSizeRangeComponent", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	if fc.HasPrevMatch && fc.PrevMatch.Defined {
		// TODO check for position, taking into account spacer/overlap
		logger.Debug("FindSizeRangeComponent: Already found, skipping", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Any("match", fc.PrevMatch))
		if fc.Check(fc.PrevMatch) {
			m := fc.PrevMatch
			(&m).fixPosition(fc.Ctx.Position)
			fc.Cm <- m
		}
		close(fc.Cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		match := Match{
			Model:    fc.ModelName,
			VarName:  fc.VarName,
			Position: fc.Ctx.Position,
			Pattern:  fc.Component.Comment,
			Defined:  false,
		}
		fc.Cm <- match
		close(fc.Cm)
		return
	}

	// Has global spacer?
	if fc.Ctx.Spacer {
		// If no prev match set as undefined
		if !fc.HasPrevMatch {
			match := Match{
				Model:    fc.ModelName,
				VarName:  fc.VarName,
				Position: fc.Ctx.Position,
				Pattern:  fc.Component.Comment,
				Defined:  false,
			}
			fc.Cm <- match
			close(fc.Cm)
			return
		}
		// If has prev match, check if next is defined or exists
		// If no next, then compute else wait for next to be defined
		// and compute spacer range from next match position
		hasNext := fc.PrevFollowingMatch.Position == uint64(fc.Ctx.Sequence.Size+1)

		if !fc.PrevFollowingMatch.Defined && hasNext { // Not yet defined, let's wait for it to be defined
			//logger.Error("has next and next not defined", zap.Any("has prev", fc.PrevFollowingMatch))
			match := Match{
				Model:    fc.ModelName,
				VarName:  fc.VarName,
				Position: fc.Ctx.Position,
				Pattern:  fc.Component.Comment,
				Defined:  false,
			}
			fc.Cm <- match
			close(fc.Cm)
			return
		}

	}

	var err error
	var match *Match

	methStartMinName := fc.ModelName + "_" + fc.VarName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.Ctx)
	if minStart > 0 {
		fc.Ctx.Position = uint64(minStart)
	}

	methMinName := fc.ModelName + "_" + fc.VarName + "SizeMin"
	methMaxName := fc.ModelName + "_" + fc.VarName + "SizeMax"
	min, minerr := CallComputeFunction(methMinName, fc.Ctx)
	max, maxerr := CallComputeFunction(methMaxName, fc.Ctx)
	if minerr != nil || maxerr != nil {
		fc.Cm <- Match{
			Model:    fc.ModelName,
			VarName:  fc.VarName,
			Position: fc.Ctx.Position,
			Pattern:  fc.Component.Comment,
			Len:      0,
			Defined:  false,
		}
		close(fc.Cm)
		return
	}
	fc.min = min
	fc.max = max
	fc.count = min

	originalPosition := fc.Ctx.Position

	maxSpacer := 0
	minSpacer := 0
	// Global spacer
	if fc.Ctx.Spacer {
		// Default, parse remaining of sequence
		maxSpacer = fc.Ctx.Sequence.Size - int(fc.Ctx.Position)
		hasNext := !(fc.PrevFollowingMatch.Position == uint64(fc.Ctx.Sequence.Size+1))
		if fc.PrevFollowingMatch.Defined && hasNext {
			// Has an other match after current and next match position is known
			methNextSpacerMinName := fc.PrevFollowingMatch.Model + "_" + fc.PrevFollowingMatch.VarName + "SpacerMin"
			methNextSpacerMaxName := fc.PrevFollowingMatch.Model + "_" + fc.PrevFollowingMatch.VarName + "SpacerMax"
			minNextSpacer, _ := CallComputeFunction(methNextSpacerMinName, fc.Ctx)
			maxNextSpacer, _ := CallComputeFunction(methNextSpacerMaxName, fc.Ctx)

			/// Set minimal start position knowning next match pos and max expected length
			fc.Ctx.Position = fc.PrevFollowingMatch.Position - uint64(max+maxNextSpacer)
			// 			fc.Ctx.Position = fc.PrevFollowingMatch.Position - uint64(max)

			// Compute max spacer, will not exceed next match position....
			maxSpacer = int(max+(maxNextSpacer-minNextSpacer)) - 1

		}
	}

	methSpacerMinName := fc.ModelName + "_" + fc.VarName + "SpacerMin"
	methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "SpacerMax"
	minVarSpacer, minerr := CallComputeFunction(methSpacerMinName, fc.Ctx)
	maxVarSpacer, maxerr := CallComputeFunction(methSpacerMaxName, fc.Ctx)
	if int(minVarSpacer) > minSpacer {
		minSpacer = int(minVarSpacer)
	}
	if int(maxVarSpacer) > maxSpacer {
		maxSpacer = int(maxVarSpacer)
	}

	//logger.Debug("spacer from/to", zap.Int("min", minSpacer), zap.Int("max", maxSpacer))
	//logger.Debug("size from/to", zap.Int64("min", fc.min), zap.Int64("max", fc.max))

	for i := minSpacer; i <= maxSpacer; i++ {
		fc.count = fc.min
		for size := fc.min; size <= fc.max; size++ {
			match, err = fc.Search()
			if err == nil {
				match.SpacerLen = match.Position - originalPosition
				if originalPosition > match.Position {
					match.SpacerLen = 0
				}
				checked, checkErr := fc.CheckConstraints(*match, true)
				if checkErr == nil && checked {
					fc.Cm <- *match
				}
			}
			fc.count++
		}
		fc.Ctx.Position++
	}
	close(fc.Cm)
}

// Search search for all possible matches
func (fc FindSizeRangeComponent) Search() (*Match, error) {
	// call function minsize maxsize from Models
	logger.Debug("FindSizeRangeComponent:Search", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))

	if fc.count > fc.max {
		return nil, fmt.Errorf("Max reached")
	}
	logger.Debug("FindSizeRangeComponent:Range:", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Int64("min", fc.min), zap.Int64("max", fc.max))

	content := fc.Ctx.SequenceLru.GetContent(int(fc.Ctx.Position), int(fc.Ctx.Position)+int(fc.count))
	if content == "" {
		return nil, fmt.Errorf("FindSizeRangeComponent:no match found")
	}

	match := Match{
		Model:    fc.ModelName,
		VarName:  fc.VarName,
		Position: fc.Ctx.Position,
		Len:      uint64(len(content)),
		Value:    content,
		Pattern:  fc.Component.Comment,
		Defined:  true,
	}
	return &match, nil
}

// CloneMatches makes a copy of an array of Match recursively
func CloneMatches(matches []Match) []Match {
	if matches == nil {
		return make([]Match, 0)
	}
	cloned := make([]Match, len(matches))
	for pi, pm := range matches {
		if len(pm.PrevFoundMatch) > 0 {
			pm.PrevFoundMatch = CloneMatches(pm.PrevFoundMatch)
		}
		if len(pm.Match) > 0 {
			pm.Match = CloneMatches(pm.Match)
		}
		cloned[pi] = pm

	}
	return cloned
}

// NewSearchComponent TODO
func NewSearchComponent(ctx Context, modelName string, varName string, hasPrevMatch bool, prevMatch Match, prevMatches []Match) SearchComponent {

	prevFollowingMatch := Match{
		Defined:  true,
		Position: uint64(ctx.Sequence.Size + 1),
	}
	if hasPrevMatch && len(prevMatches) > 0 {
		prevFollowingMatch = prevMatches[0]
	}

	if ctx.Grammar.Models[modelName].Vars[varName].Model.Name != "" {
		// Call an other model
		logger.Debug("Component:FindModelComponent", zap.String("model", modelName), zap.String("var", varName))
		fc := FindModelComponent{}
		fc.Position = ctx.Position
		fc.ModelName = modelName
		fc.VarName = varName
		fc.HasPrevMatch = hasPrevMatch
		fc.PrevMatch = prevMatch
		//fc.PrevMatches = CloneMatches(prevMatches)
		fc.Cm = make(chan Match)
		if ctx.Grammar != nil {
			fc.Component = ctx.Grammar.Models[modelName].Vars[varName]
		}
		fc.Ctx = ctx
		return fc
	} else if ctx.Grammar.Models[modelName].Vars[varName].Value != "" || ctx.Grammar.Models[modelName].Vars[varName].StringConstraints.Content != "" {
		variable := ctx.Grammar.Models[modelName].Vars[varName]
		hasSpacer := false
		if ctx.Spacer {
			hasSpacer = true
		}
		if ctx.Grammar.Models[modelName].Vars[varName].Spacer {
			hasSpacer = true
		}
		if hasSpacer {
			fc := FindCassieComponent{}
			fc.Position = ctx.Position
			fc.HasPrevMatch = hasPrevMatch
			fc.PrevMatch = prevMatch
			//fc.PrevMatches = CloneMatches(prevMatches)
			fc.ModelName = modelName
			fc.VarName = varName
			fc.Cm = make(chan Match)
			if ctx.Grammar != nil {
				fc.Component = ctx.Grammar.Models[modelName].Vars[varName]
			}
			fc.Ctx = ctx
			return fc
		}

		if variable.StructConstraints.Cost.Max != "" || variable.StructConstraints.Distance.Max != "" {
			logger.Debug("Component:FindApproximateComponent", zap.String("model", modelName), zap.String("var", varName))
			fc := FindApproximateComponent{}
			fc.Position = ctx.Position
			fc.HasPrevMatch = hasPrevMatch
			fc.PrevMatch = prevMatch
			//fc.PrevMatches = CloneMatches(prevMatches)
			fc.ModelName = modelName
			fc.VarName = varName
			fc.Cm = make(chan Match)
			if ctx.Grammar != nil {
				fc.Component = ctx.Grammar.Models[modelName].Vars[varName]
			}
			fc.Ctx = ctx
			return fc
		}
		// Exact  match
		logger.Debug("Component:FindExactComponent", zap.String("model", modelName), zap.String("var", varName))
		fc := FindExactComponent{}
		fc.Position = ctx.Position
		fc.HasPrevMatch = hasPrevMatch
		fc.PrevMatch = prevMatch
		//fc.PrevMatches = CloneMatches(prevMatches)
		fc.ModelName = modelName
		fc.VarName = varName
		fc.Cm = make(chan Match)
		if ctx.Grammar != nil {
			fc.Component = ctx.Grammar.Models[modelName].Vars[varName]
		}
		fc.Ctx = ctx
		return fc
	} else if ctx.Grammar.Models[modelName].Vars[varName].StringConstraints.Size.Min != "" && ctx.Grammar.Models[modelName].Vars[varName].StringConstraints.Size.Max != "" {
		// Size based variable
		logger.Debug("Component:SizeRangeComponent", zap.String("model", modelName), zap.String("var", varName))
		fc := FindSizeRangeComponent{}
		fc.Position = ctx.Position
		fc.HasPrevMatch = hasPrevMatch
		fc.PrevMatch = prevMatch
		// fc.PrevMatches = CloneMatches(prevMatches)
		fc.PrevFollowingMatch = prevFollowingMatch
		fc.ModelName = modelName
		fc.VarName = varName
		fc.Cm = make(chan Match)
		if ctx.Grammar != nil {
			fc.Component = ctx.Grammar.Models[modelName].Vars[varName]
		}
		fc.Ctx = ctx
		return fc
	}
	panic(fmt.Sprintf("Failed to find a component, exiting, %+v", ctx.Grammar.Models[modelName].Vars[varName]))

}

// FindNotMatches search for all matches within size range not matching previous search stored in foundMatches
func (fc FindExactComponent) FindNotMatches(start uint64, foundMatches map[string]bool, ch chan Match) {
	if fc.Component.Not {
		fail := false
		originalPosition := start
		methMinName := fc.ModelName + "_" + fc.VarName + "SizeMin"
		methMaxName := fc.ModelName + "_" + fc.VarName + "SizeMax"
		minSize, minerr := CallComputeFunction(methMinName, fc.Ctx)
		maxSize, maxerr := CallComputeFunction(methMaxName, fc.Ctx)
		if minerr != nil || maxerr != nil {
			fail = true
			logger.Error("Failed to evaluate conditions", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Error(minerr), zap.Error(maxerr))
		}

		var minSpacer int64
		var maxSpacer int64

		methOverlapMaxName := fc.ModelName + "_" + fc.VarName + "OverlapMax"
		maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.Ctx)
		originalPosition = originalPosition - uint64(maxOverlap)

		// Global spacer
		if fc.Ctx.Spacer {
			maxSpacer = int64(fc.Ctx.Sequence.Size) - int64(fc.Ctx.Position)
		} else {
			if fc.Ctx.IsRepeat {
				methSpacerMinName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMin"
				methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMax"
				minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.Ctx)
				maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.Ctx)
				minSpacer = minSpacerComponent
				maxSpacer = maxSpacerComponent
				methOverlapMaxName := fc.ModelName + "_" + fc.VarName + "RepeatOverlapMax"
				maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.Ctx)
				originalPosition = originalPosition - uint64(maxOverlap)
			} else {
				methSpacerMinName := fc.ModelName + "_" + fc.VarName + "SpacerMin"
				methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "SpacerMax"
				minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.Ctx)
				maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.Ctx)
				minSpacer = minSpacerComponent
				maxSpacer = maxSpacerComponent
			}
		}

		if minSpacer > 0 {
			originalPosition += uint64(minSpacer)
		}

		if !fail {
			logger.Debug("found", zap.Any("matches", foundMatches))
			logger.Debug("Search", zap.Uint64("start", originalPosition), zap.Int64("end", maxOverlap+maxSpacer))
			logger.Debug("Size", zap.Int64("min", minSize), zap.Int64("max", maxSize))

			for pos := 0; pos <= int(maxOverlap+maxSpacer); pos++ {
				for i := minSize; i <= maxSize; i++ {
					matchpos := originalPosition + uint64(pos)
					if _, ok := foundMatches[fmt.Sprintf("%d-%d", matchpos, i)]; ok {
						continue
					}
					content := fc.Ctx.SequenceLru.GetContent(int(matchpos), int(matchpos)+int(i))
					match := Match{
						Model:          fc.ModelName,
						VarName:        fc.VarName,
						Value:          content,
						Position:       matchpos,
						Len:            uint64(i),
						Vars:           fc.Ctx.Vars,
						PrevFoundMatch: CloneMatches(fc.PrevMatch.PrevFoundMatch),
						Defined:        true,
					}

					// No need to check match, as we take all not matching previous search
					// checked, checkErr := fc.CheckConstraints(match, true)
					fc.Cm <- match
				}
			}
		}
	}

}

/*
// GetPrevMatches returns matches previsouly found
func (fc FindExactComponent) GetPrevMatches() []Match {
	return fc.PrevMatches
}*/

// CheckConstraints checks if match constraints can be evaluated and, if eval is true, checks constraints
func (fc FindExactComponent) CheckConstraints(match Match, eval bool) (bool, error) {
	logger.Debug("CheckConstraints", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Bool("eval", eval))
	methSpacerMinName := fc.ModelName + "_" + fc.VarName + "SpacerMin"
	methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "SpacerMax"
	minSpacer, minSpacerErr := CallComputeFunction(methSpacerMinName, fc.Ctx)
	maxSpacer, maxSpacerErr := CallComputeFunction(methSpacerMaxName, fc.Ctx)
	if minSpacerErr != nil || maxSpacerErr != nil {
		return false, fmt.Errorf("Cannot determine spacer")
	}

	methStartMinName := fc.ModelName + "_" + fc.VarName + "StartMin"
	methStartMaxName := fc.ModelName + "_" + fc.VarName + "StartMax"
	minStart, minStartErr := CallComputeFunction(methStartMinName, fc.Ctx)
	maxStart, maxStartErr := CallComputeFunction(methStartMaxName, fc.Ctx)
	if minStartErr != nil || maxStartErr != nil {
		return false, fmt.Errorf("Cannot determine start")
	}

	methEndMinName := fc.ModelName + "_" + fc.VarName + "EndMin"
	methEndMaxName := fc.ModelName + "_" + fc.VarName + "EndMax"
	minEnd, minEndErr := CallComputeFunction(methEndMinName, fc.Ctx)
	maxEnd, maxEndErr := CallComputeFunction(methEndMaxName, fc.Ctx)
	if minEndErr != nil || maxEndErr != nil {
		return false, fmt.Errorf("Cannot determine end")
	}

	methSizeMinName := fc.ModelName + "_" + fc.VarName + "SizeMin"
	methSizeMaxName := fc.ModelName + "_" + fc.VarName + "SizeMax"
	minSize, minSizeErr := CallComputeFunction(methSizeMinName, fc.Ctx)
	maxSize, maxSizeErr := CallComputeFunction(methSizeMaxName, fc.Ctx)
	if minSizeErr != nil || maxSizeErr != nil {
		return false, fmt.Errorf("Cannot determine size")
	}

	methCostMinName := fc.ModelName + "_" + fc.VarName + "CostMin"
	methCostMaxName := fc.ModelName + "_" + fc.VarName + "CostMax"
	minCost, minCostErr := CallComputeFunction(methCostMinName, fc.Ctx)
	maxCost, maxCostErr := CallComputeFunction(methCostMaxName, fc.Ctx)
	if minCostErr != nil || maxCostErr != nil {
		return false, fmt.Errorf("Cannot determine cost")
	}

	methDistanceMinName := fc.ModelName + "_" + fc.VarName + "DistanceMin"
	methDistanceMaxName := fc.ModelName + "_" + fc.VarName + "DistanceMax"
	minDistance, minDistanceErr := CallComputeFunction(methDistanceMinName, fc.Ctx)
	maxDistance, maxDistanceErr := CallComputeFunction(methDistanceMaxName, fc.Ctx)
	if minDistanceErr != nil || maxDistanceErr != nil {
		return false, fmt.Errorf("Cannot determine distance")
	}

	methRepeatMinName := fc.ModelName + "_" + fc.VarName + "RepeatMin"
	methRepeatMaxName := fc.ModelName + "_" + fc.VarName + "RepeatMax"
	_, minRepeatErr := CallComputeFunction(methRepeatMinName, fc.Ctx)
	maxRepeat, maxRepeatErr := CallComputeFunction(methRepeatMaxName, fc.Ctx)
	if minRepeatErr != nil || maxRepeatErr != nil {
		return false, fmt.Errorf("Cannot determine repeat")
	}

	methRepeatSpacerMinName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMin"
	methRepeatSpacerMaxName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMax"
	minRepeatSpacer, minRepeatSpacerErr := CallComputeFunction(methRepeatSpacerMinName, fc.Ctx)
	maxRepeatSpacer, maxRepeatSpacerErr := CallComputeFunction(methRepeatSpacerMaxName, fc.Ctx)
	if minRepeatSpacerErr != nil || maxRepeatSpacerErr != nil {
		return false, fmt.Errorf("Cannot determine repeat spacer")
	}

	methRepeatOverlapMinName := fc.ModelName + "_" + fc.VarName + "RepeatOverlapMin"
	methRepeatOverlapMaxName := fc.ModelName + "_" + fc.VarName + "RepeatOverlapMax"
	minRepeatOverlap, minRepeatOverlapErr := CallComputeFunction(methRepeatOverlapMinName, fc.Ctx)
	maxRepeatOverlap, maxRepeatOverlapErr := CallComputeFunction(methRepeatOverlapMaxName, fc.Ctx)
	if minRepeatOverlapErr != nil || maxRepeatOverlapErr != nil {
		return false, fmt.Errorf("Cannot determine repeat spacer")
	}

	methOverlapMinName := fc.ModelName + "_" + fc.VarName + "OverlapMin"
	methOverlapMaxName := fc.ModelName + "_" + fc.VarName + "OverlapMax"
	_, minOverlapErr := CallComputeFunction(methOverlapMinName, fc.Ctx)
	_, maxOverlapErr := CallComputeFunction(methOverlapMaxName, fc.Ctx)
	if minOverlapErr != nil || maxOverlapErr != nil {
		return false, fmt.Errorf("Cannot determine overlap")
	}

	if !eval {
		return true, nil
	}
	if minSpacer > 0 && (match.Position-fc.Position) < uint64(minSpacer) {
		logger.Debug("minSpacer control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("spacer", match.SpacerLen), zap.Int64("min", minSpacer))
		return false, nil
	}
	if maxSpacer > 0 && (match.Position-fc.Position) > uint64(maxSpacer) {
		logger.Debug("maxSpacer control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("spacer", match.SpacerLen), zap.Int64("max", maxSpacer))
		return false, nil
	}

	if minStart > 0 && match.Position < uint64(minStart) {
		logger.Debug("minStart control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("position", match.Position), zap.Int64("min", minStart))
		return false, nil
	}
	if maxStart > 0 && match.Position > uint64(maxStart) {
		logger.Debug("maxSpacer control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("position", match.Position), zap.Int64("max", maxStart))
		return false, nil
	}

	if minEnd > 0 && match.Position+match.Len < uint64(minEnd) {
		logger.Debug("minEnd control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("position", match.Position+match.Len), zap.Int64("min", minEnd))
		return false, nil
	}
	if maxEnd > 0 && match.Position+match.Len > uint64(maxEnd) {
		logger.Debug("maxEnd control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("position", match.Position+match.Len), zap.Int64("max", maxEnd))
		return false, nil
	}

	if !fc.Component.Not && minSize > 0 && match.Len < uint64(minSize) {
		logger.Debug("minSize control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("size", match.Len), zap.Int64("min", minSize))
		return false, nil
	}
	if !fc.Component.Not && maxSize > 0 && match.Len > uint64(maxSize) {
		logger.Debug("maxSize control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("size", match.Len), zap.Int64("max", maxSize))
		return false, nil
	}

	if minCost > 0 && match.Cost < uint64(minCost) {
		logger.Debug("minCost control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("cost", match.Cost), zap.Int64("min", minCost))
		return false, nil
	}
	if maxCost > 0 && match.Cost > uint64(maxCost) {
		logger.Debug("maxCost control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("cost", match.Cost), zap.Int64("max", maxCost))
		return false, nil
	}

	if minDistance > 0 && match.Distance < uint64(minDistance) {
		logger.Debug("minDist control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("distance", match.Distance), zap.Int64("min", minDistance))
		return false, nil
	}
	if maxDistance > 0 && match.Distance > uint64(maxDistance) {
		logger.Debug("maxDist control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("distance", match.Distance), zap.Int64("max", maxDistance))
		return false, nil
	}

	// Check max repeat only here
	if maxRepeat > 0 && match.RepeatIndex > uint64(maxRepeat) {
		logger.Debug("maxRepeat control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("repeat", match.RepeatIndex), zap.Int64("max", maxRepeat))
		return false, nil
	}

	if match.RepeatIndex > 0 {
		// Check repeats
		if minRepeatSpacer > 0 && match.SpacerLen < uint64(minRepeatSpacer) {
			logger.Debug("minRepeatSpacer control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("spacer", match.SpacerLen), zap.Int64("min", minRepeatSpacer))
			return false, nil
		}
		if maxRepeatSpacer > 0 && match.SpacerLen > uint64(maxRepeatSpacer) {
			logger.Debug("maxRepeatSpacer control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("spacer", match.SpacerLen), zap.Int64("max", maxRepeatSpacer))
			return false, nil
		}
		if minRepeatOverlap > 0 && match.OverlapLen < uint64(minRepeatOverlap) {
			logger.Debug("minRepeatOverlap control failed <", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("overlap", match.OverlapLen), zap.Int64("min", minRepeatOverlap))
			return false, nil
		}
		if maxRepeatOverlap > 0 && match.OverlapLen < uint64(maxRepeatOverlap) {
			logger.Debug("maxRepeatOverlap control failed >", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Uint64("overlap", match.OverlapLen), zap.Int64("max", maxRepeatOverlap))
			return false, nil
		}
	}

	logger.Debug("CheckConstraints:OK", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	return true, nil

}

// Check verifies match position against ctx position and constraints
func (fc FindExactComponent) Check(m Match) bool {
	if fc.Ctx.Spacer {
		return true
	}
	// Component spacer
	spacer := false
	methMinName := fc.ModelName + "_" + fc.VarName + "SpacerMin"
	methMaxName := fc.ModelName + "_" + fc.VarName + "SpacerMax"
	minSize, _ := CallComputeFunction(methMinName, fc.Ctx)
	maxSize, _ := CallComputeFunction(methMaxName, fc.Ctx)
	if minSize > 0 && m.Position < fc.Ctx.Position+uint64(minSize) {
		logger.Debug("Check on spacer failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}
	if maxSize > 0 && m.Position > fc.Ctx.Position+uint64(maxSize) {
		logger.Debug("Check on spacer failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}
	if minSize > 0 || maxSize > 0 {
		spacer = true
	}

	// Component overlap
	overlap := false
	methMinName = fc.ModelName + "_" + fc.VarName + "OverlapMin"
	methMaxName = fc.ModelName + "_" + fc.VarName + "OverlapMax"
	minSize, _ = CallComputeFunction(methMinName, fc.Ctx)
	maxSize, _ = CallComputeFunction(methMaxName, fc.Ctx)
	if minSize > 0 && m.Position > fc.Ctx.Position-uint64(minSize) {
		logger.Debug("Check on overlap failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}
	if maxSize > 0 && m.Position < fc.Ctx.Position-uint64(maxSize) {
		logger.Debug("Check on overlap failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}
	if minSize > 0 || maxSize > 0 {
		overlap = true
	}

	methStartMinName := fc.ModelName + "_" + fc.VarName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.Ctx)
	if fc.Ctx.Position < uint64(minStart) {
		logger.Debug("Check on min start failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}
	methStartMaxName := fc.ModelName + "_" + fc.VarName + "StartMax"
	maxStart, _ := CallComputeFunction(methStartMaxName, fc.Ctx)
	if fc.Ctx.Position < uint64(maxStart) {
		logger.Debug("Check on max start failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}

	if m.RepeatIndex > 0 {
		// This is a repeated element, check repeat spacer/overlap
		methMinName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMin"
		methMaxName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMax"
		minSize, _ := CallComputeFunction(methMinName, fc.Ctx)
		maxSize, _ := CallComputeFunction(methMaxName, fc.Ctx)
		if minSize > 0 && m.Position < fc.Ctx.Position+uint64(minSize) {
			logger.Debug("Check on spacer failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
			return false
		}
		if maxSize > 0 && m.Position > fc.Ctx.Position+uint64(maxSize) {
			logger.Debug("Check on spacer failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
			return false
		}
		if minSize > 0 || maxSize > 0 {
			spacer = true
		}

		// Component overlap
		methMinName = fc.ModelName + "_" + fc.VarName + "RepeatOverlapMin"
		methMaxName = fc.ModelName + "_" + fc.VarName + "RepeatOverlapMax"
		minSize, _ = CallComputeFunction(methMinName, fc.Ctx)
		maxSize, _ = CallComputeFunction(methMaxName, fc.Ctx)
		if minSize > 0 && m.Position > fc.Ctx.Position-uint64(minSize) {
			logger.Debug("Check on overlap failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
			return false
		}
		if maxSize > 0 && m.Position < fc.Ctx.Position-uint64(maxSize) {
			logger.Debug("Check on overlap failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
			return false
		}
		if minSize > 0 || maxSize > 0 {
			overlap = true
		}
	}

	if !spacer && !overlap && fc.Ctx.Position != m.Position {
		logger.Debug("Check on position failed", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
		return false
	}

	logger.Debug("Match do not match on position, skipping", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Any("match", m))
	return true
}

// GetComponent TODO
func (fc FindExactComponent) GetComponent() Variable {
	return fc.Component
}

// Context returns ctx element
func (fc FindExactComponent) Context() Context {
	return fc.Ctx
}

// Search TODO
func (fc FindExactComponent) Search() (*Match, error) {
	logger.Debug("FindExactComponent:Search", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))

	constraint := fc.Component.Value

	if fc.Component.StringConstraints.Content != "" {
		ctxVar, ok := fc.Ctx.Vars[fc.Component.StringConstraints.Content]
		if !ok {
			match := Match{
				Model:    fc.ModelName,
				VarName:  fc.VarName,
				Position: fc.Ctx.Position,
				// TODO could check if relates to a constant string or size constrained var to set min size
				Len:     0,
				Pattern: fc.Component.Comment,
				Defined: false,
			}
			return &match, nil
		}
		constraint = fc.Ctx.SequenceLru.GetContent(int(ctxVar.Position), int(ctxVar.Position+ctxVar.Len))
	}

	if constraint == "" {
		return nil, fmt.Errorf("No content found")
	}

	content := fc.Ctx.SequenceLru.GetContent(int(fc.Ctx.Position), int(fc.Ctx.Position)+len(constraint))
	if content == "" {
		return nil, fmt.Errorf("FindExactComponent:no match found")
	}
	logger.Debug("FindExactComponent:Compare", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.String("constraint", constraint), zap.String("content", content))

	seq1 := &DnaString{}
	seq1.SetValue(constraint)
	seq2 := &DnaString{}
	seq2.SetValue(content)
	isEqual := false
	if fc.Component.StringConstraints.Morphism != "" {
		morphseq1, morphErr := seq1.Morph(fc.Component.StringConstraints.Morphism, fc.Ctx.Morphisms, fc.Component.StringConstraints.Reverse)
		if morphErr != nil {
			return nil, morphErr
		}
		isEqual = morphseq1.IsExact(seq2)
	} else {
		isEqual = seq1.IsExact(seq2)
	}

	if isEqual {
		match := Match{
			Model:    fc.ModelName,
			VarName:  fc.VarName,
			Position: fc.Ctx.Position,
			Len:      uint64(len(content)),
			Value:    content,
			Pattern:  fc.Component.Comment,
			Defined:  true,
		}
		logger.Debug("FindExactComponent:Got match", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Any("match", match))
		return &match, nil
	}
	return nil, fmt.Errorf("FindExactComponent:no match found")
}

// SaveAs tells us if component should save variable and with which variable name
func (fc FindExactComponent) SaveAs() (string, bool) {
	if fc.Component.StringConstraints.SaveAs != "" {
		return fc.Component.StringConstraints.SaveAs, true
	}
	return "", false
}

// Iterator returns a chan to receive matches
func (fc FindExactComponent) Iterator() (cm chan Match) {
	return fc.Cm
}

// Items loop over possible matches and send matches to iterator chan
func (fc FindExactComponent) Items() {
	logger.Debug("FindExactComponent", zap.String("model", fc.ModelName), zap.String("var", fc.VarName))
	if fc.HasPrevMatch && fc.PrevMatch.Defined {
		logger.Debug("FindExactComponent: Already found, skipping", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Any("match", fc.PrevMatch))
		if fc.Check(fc.PrevMatch) {
			m := fc.PrevMatch
			(&m).fixPosition(fc.Ctx.Position)
			fc.Cm <- m
		}
		close(fc.Cm)
		return
	}

	_, checkErr := fc.CheckConstraints(Match{}, false)
	if checkErr != nil {
		match := Match{
			Model:    fc.ModelName,
			VarName:  fc.VarName,
			Position: fc.Ctx.Position,
			Pattern:  fc.Component.Comment,
			Defined:  false,
		}
		fc.Cm <- match
		close(fc.Cm)
		return
	}

	// TODO not efficient spacer use for exact match, should use cassiopee
	var maxSpacer int64
	var minSpacer int64

	methStartMinName := fc.ModelName + "_" + fc.VarName + "StartMin"
	minStart, _ := CallComputeFunction(methStartMinName, fc.Ctx)
	if minStart > 0 {
		fc.Ctx.Position = uint64(minStart)
	}

	originalPosition := fc.Ctx.Position
	foundMatches := make(map[string]bool)

	methOverlapMaxName := fc.ModelName + "_" + fc.VarName + "OverlapMax"
	maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.Ctx)
	fc.Ctx.Position -= uint64(maxOverlap)

	// Global spacer
	if fc.Ctx.Spacer {
		maxSpacer = int64(fc.Ctx.Sequence.Size) - int64(fc.Ctx.Position)
	} else {
		if fc.Ctx.IsRepeat {
			methSpacerMinName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMin"
			methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "RepeatSpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.Ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.Ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
			methOverlapMaxName := fc.ModelName + "_" + fc.VarName + "RepeatOverlapMax"
			maxOverlap, _ := CallComputeFunction(methOverlapMaxName, fc.Ctx)
			fc.Ctx.Position = originalPosition - uint64(maxOverlap)
		} else {
			methSpacerMinName := fc.ModelName + "_" + fc.VarName + "SpacerMin"
			methSpacerMaxName := fc.ModelName + "_" + fc.VarName + "SpacerMax"
			minSpacerComponent, _ := CallComputeFunction(methSpacerMinName, fc.Ctx)
			maxSpacerComponent, _ := CallComputeFunction(methSpacerMaxName, fc.Ctx)
			minSpacer = minSpacerComponent
			maxSpacer = maxSpacerComponent
		}
	}
	if minSpacer > 0 {
		fc.Ctx.Position += uint64(minSpacer)
	}

	logger.Debug("FindExactComponent:Spacer", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Int64("maxspacer", maxSpacer))
	for i := 0; i <= int(maxOverlap+maxSpacer); i++ {
		match, err := fc.Search()
		if err != nil {
			logger.Debug("no match found", zap.String("model", fc.ModelName), zap.String("var", fc.VarName), zap.Error(err))
		} else {
			match.SpacerLen = match.Position - originalPosition
			if originalPosition > match.Position {
				match.SpacerLen = 0
			}
			if match.Position < originalPosition {
				match.OverlapLen = originalPosition - match.Position
			}
			checked, checkErr := fc.CheckConstraints(*match, true)
			if checkErr == nil && checked {
				if fc.Component.Not {
					foundMatches[fmt.Sprintf("%d-%d", match.Position, match.Len)] = true
				} else {
					fc.Cm <- *match
				}
			}
		}
		fc.Ctx.Position++
	}

	// Manage not condition
	if fc.Component.Not {
		fc.FindNotMatches(originalPosition, foundMatches, fc.Cm)
	}

	close(fc.Cm)
}

// Stat contains stat info for progress
type Stat struct {
	Trigger uint64
	Matches uint64
}

var progress = make(map[string]*Stat)

// TrackProgress increment number of input message per component
func TrackProgress(p chan string) {
	for pg := range p {
		stat := strings.Split(pg, ":")
		if _, ok := progress[stat[0]]; !ok {
			progress[stat[0]] = &Stat{}
		}
		if stat[1] == "T" {
			progress[stat[0]].Trigger++
		} else if stat[1] == "M" {
			progress[stat[0]].Matches++
		}
	}
}

// GetProgress returns progress count
func GetProgress() map[string]*Stat {
	return progress
}

func progressHandler(w http.ResponseWriter, r *http.Request) {
	js, err := json.Marshal(progress)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func modelHandler(w http.ResponseWriter, r *http.Request) {
	page := `
	<div id="container" style="height:400px; margin: auto; max-width:800px"></div>
	<script src="https://cdn.jsdelivr.net/npm/sigma@1.2.1/build/sigma.require.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sigma@1.2.1/plugins/sigma.layout.forceAtlas2/supervisor.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sigma@1.2.1/plugins/sigma.layout.forceAtlas2/worker.js"></script>
	<script>
	`
	page += "data = " + graphJSON + ";\n"
	page += `
	var s = new sigma(
		{
		  renderer: {
			container: document.getElementById('container'),
			type: 'canvas'
		  },
		  settings: {
			minArrowSize: 10
		  }
		}
	  );
	  // load the graph
		s.graph.read(data);
		// draw the graph
		s.refresh();
		s.startForceAtlas2();
        window.setTimeout(function() {s.killForceAtlas2()}, 1000);
	</script>
	
	`
	//w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(page))
}

var graphJSON string

// ServeProgress starts a background web server to query progress
func ServeProgress(graph string) {
	graphJSON = graph
	http.HandleFunc("/model", modelHandler)
	http.HandleFunc("/progress", progressHandler)
	port := os.Getenv("LOGOL_LISTEN") // ":8080 for example"
	if port != "" {
		log.Fatal(http.ListenAndServe(port, nil))
	}
}

// MergeRepeats merge a match with its previous repeat matches
func MergeRepeats(ctx Context, msg Match, prevMsg []Match) Match {
	if len(prevMsg) == 0 {
		return msg
	}
	allMatches := append(prevMsg, msg)
	newMatch := msg
	newMatch.Spacer = prevMsg[0].Spacer
	newMatch.SpacerLen = prevMsg[0].SpacerLen
	newMatch.Position = prevMsg[0].Position
	newMatch.Len = 0
	newMatch.Cost = 0
	newMatch.Distance = 0
	newMatch.Match = make([]Match, 0)
	for _, elt := range allMatches {
		newMatch.Len += elt.Len
		newMatch.Cost += elt.Cost
		newMatch.Distance += elt.Distance
	}
	newMatch.Value = ctx.SequenceLru.GetContent(int(newMatch.Position), int(newMatch.Position+newMatch.Len))
	return newMatch
}

// MergeMatches merges multiple match results in a single match
func MergeMatches(msg Match, ctx Context, component Variable) Match {
	firstMatch := msg.Match[0]
	lastMatch := msg.Match[len(msg.Match)-1]
	var endPosition uint64
	if lastMatch.Defined {
		endPosition = lastMatch.Position + lastMatch.Len
	}
	position := msg.Position
	if firstMatch.Defined {
		position = firstMatch.Position
	}
	var matchLen uint64
	content := ""
	if firstMatch.Defined && lastMatch.Defined {
		matchLen = endPosition - position
		content = ctx.SequenceLru.GetContent(int(firstMatch.Position), int(lastMatch.Position+lastMatch.Len))
	}

	// Check if all sub elements are defined
	allDefined := true
	var cost uint64
	var distance uint64
	for _, subm := range msg.Match {
		if !subm.Defined {
			allDefined = false
			break
		}
		cost += subm.Cost
		distance += subm.Distance
	}

	match := Match{
		Position:    position,
		Len:         matchLen,
		Value:       content,
		Defined:     allDefined,
		Pattern:     component.Comment,
		Match:       msg.Match,
		Spacer:      firstMatch.Spacer,
		SpacerLen:   firstMatch.SpacerLen,
		Cost:        cost,
		Distance:    distance,
		RepeatIndex: msg.RepeatIndex,
	}

	match.Vars = make(map[string]Match)
	for k, v := range ctx.Vars {
		match.Vars[k] = v
	}
	match.PrevVars = make(map[string]Match)
	if msg.PrevVars != nil {
		for k, v := range msg.PrevVars {
			match.PrevVars[k] = v
		}
	}
	for k, v := range lastMatch.Vars {
		match.PrevVars[k] = v
	}

	return match
}

// CallComputeFunction calls method defined by name
// *name* must be defined as a function in *Models*
func CallComputeFunction(name string, ctx Context) (int64, error) {
	logger.Debug("CallComputeFunction", zap.String("name", name))
	fn, ok := ctx.Models[name]
	if ok {
		fnc := reflect.ValueOf(fn)
		in := make([]reflect.Value, 1)
		in[0] = reflect.ValueOf(ctx)
		values := fnc.Call(in)
		err := values[1].Interface()
		if err == nil {
			return values[0].Int(), nil
		}
		logger.Error("Failed to evaluate", zap.String("model", name))
		return 0, fmt.Errorf("Failed to evaluate %s", name)

	}
	return 0, fmt.Errorf("could not find requested method %s", name)
}

// CallCompareFunction calls method defined by name
// *name* must be defined as a function in *Models*
func CallCompareFunction(name string, ctx Context) (bool, error) {
	logger.Debug("CallCompareFunction", zap.String("name", name))
	fn, ok := ctx.Models[name]
	if ok {
		fnc := reflect.ValueOf(fn)
		in := make([]reflect.Value, 1)
		in[0] = reflect.ValueOf(ctx)
		values := fnc.Call(in)
		err := values[1].Interface()
		if err == nil {
			return values[0].Bool(), nil
		}
		logger.Debug("Failed to evaluate", zap.String("model", name), zap.Any("err", err))
		return false, fmt.Errorf("Failed to evaluate %s", name)

	}
	return false, fmt.Errorf("could not find requested method %s", name)
}

// EvalComputeFunction generate code to evaluate a compte string (vars.R1.Len + 2 for example)
func EvalComputeFunction(compute string) string {
	if compute == "" {
		return "return 0, nil"
	}
	res := ""
	re := regexp.MustCompile(`vars\.(\w+).`)
	matches := re.FindAllSubmatch([]byte(compute), -1)
	if matches == nil {
		return "\nreturn int64(" + compute + "), nil\n"
	}
	for _, submatch := range matches {
		match := submatch[1]
		res += fmt.Sprintf(`
    if _, ok := ctx.Vars["%s"]; !ok {
      return 0, fmt.Errorf("variable not found")
    }
`, string(match))
		compute = strings.ReplaceAll(compute, "vars."+string(match), "ctx.Vars[\""+string(match)+"\"]")
	}
	res += "\nreturn int64(" + compute + "), nil\n"
	return res
}

// EvalCompareFunction generate code to evaluate a string condition (vars.R1.Len > 2 for example)
func EvalCompareFunction(compute string) string {
	if compute == "" {
		return "return true, nil"
	}
	res := ""
	re := regexp.MustCompile(`vars\.(\w+).`)
	matches := re.FindAllSubmatch([]byte(compute), -1)
	if matches == nil {
		return "\nreturn true, nil\n"
	}
	for _, submatch := range matches {
		match := submatch[1]
		res += fmt.Sprintf(`
    if _, ok := ctx.Vars["%s"]; !ok {
      return false, fmt.Errorf("variable not found")
    }
`, string(match))
		compute = strings.ReplaceAll(compute, "vars."+string(match), "ctx.Vars[\""+string(match)+"\"]")
	}
	res += fmt.Sprintf("\n    return (%s), nil\n", compute)
	return res
}

// HasUndef checks if match has undefined variables
func HasUndef(m *Match) bool {
	if m.Defined {
		fmt.Printf("Defined, skip %+v\n", m)
		return false
	}
	hasUndef := false
	if len(m.Match) > 0 {
		for i := 0; i < len(m.Match); i++ {
			subm := &m.Match[i]
			subundef := HasUndef(subm)
			if subundef {
				hasUndef = true
				break
			}
		}
	}
	if hasUndef {
		// m.PrevFoundMatch = m.Match
		m.PrevFoundMatch = CloneMatches(m.Match)
		m.Match = nil
	}
	return true
}

// GetUndef copies found matches to PrevFoundMatch for a new analysis run
func GetUndef(m *Match) {
	m.PrevFoundMatch = make([]Match, len(m.Match))
	for i := 0; i < len(m.Match); i++ {
		subm := m.Match[i]
		GetUndef(&subm)
		m.PrevFoundMatch[i] = subm
	}
	m.Match = make([]Match, 0)
}
