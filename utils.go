package logol

import (
	"archive/zip"
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

// DeprecatedSwitchCtxVars take context vars and map them with main model vars
// back param defines switching direction
func _DeprecatedSwitchCtxVars(model string, ctx Context, vars map[string]Match, back bool) map[string]Match {
	newVars := make(map[string]Match)

	var mainModel ModelDef
	for _, mdef := range ctx.Grammar.Run {
		if mdef.Model == model {
			mainModel = mdef
			break
		}
	}
	calledModel := ctx.Grammar.Models[model]
	calledModelParams := calledModel.Param

	if len(calledModelParams) != len(mainModel.Param) {
		logger.Error("Input and output parameters do not match", zap.String("model", model))
		return newVars
	}

	for index, param := range calledModelParams {
		callingVar := mainModel.Param[index]
		if back {
			callingVarDetails, ok := vars[param]
			if ok {
				newVars[callingVar] = callingVarDetails
			}
		} else {
			callingVarDetails, ok := vars[callingVar]
			if ok {
				newVars[param] = callingVarDetails
			}
		}

	}
	return newVars
}

// SwitchCtxFromToVars switch context vars between two models params
func SwitchCtxFromToVars(model string, callingParams []string, calledParams []string, vars map[string]Match, prevVars map[string]Match, back bool) map[string]Match {
	newVars := make(map[string]Match)

	if len(calledParams) != len(callingParams) {
		logger.Error("Input and output parameters do not match", zap.String("model", model), zap.Any("from", callingParams), zap.Any("to", calledParams))
		panic("Input and output parameters do not match")
		//return newVars
	}

	logger.Debug("switch model params", zap.String("model", model), zap.Any("calling", callingParams), zap.Any("called", calledParams), zap.Bool("back", back))

	// Save model saved context variable if any
	if !back {
		if prevVars != nil {
			for k, v := range prevVars {
				newVars[k] = v
			}
		}
	}

	for index, param := range calledParams {
		callingVar := callingParams[index]
		if back {
			callingVarDetails, ok := vars[param]
			if ok {
				newVars[callingVar] = callingVarDetails
			}
		} else {
			callingVarDetails, ok := vars[callingVar]
			if ok {
				newVars[param] = callingVarDetails
			}
		}
	}
	return newVars

}

// SwitchCtxVars take context vars and map them with main model vars
// back param defines switching direction
func SwitchCtxVars(model string, ctx Context, vars map[string]Match, prevVars map[string]Match, back bool) map[string]Match {
	//newVars := make(map[string]Match)

	var mainModel ModelDef
	for _, mdef := range ctx.Grammar.Run {
		if mdef.Model == model {
			mainModel = mdef
			break
		}
	}
	calledModel := ctx.Grammar.Models[model]
	calledModelParams := calledModel.Param

	return SwitchCtxFromToVars(model, mainModel.Param, calledModelParams, vars, prevVars, back)

	/*
		if len(calledModelParams) != len(mainModel.Param) {
			logger.Error("Input and output parameters do not match", zap.String("model", model))
			return newVars
		}

		// Save model saved context variable if any
		if !back {
			if prevVars != nil {
				for k, v := range prevVars {
					newVars[k] = v
				}
			}
		} else {

		}

		for index, param := range calledModelParams {
			callingVar := mainModel.Param[index]
			if back {
				callingVarDetails, ok := vars[param]
				if ok {
					newVars[callingVar] = callingVarDetails
				}
			} else {
				callingVarDetails, ok := vars[callingVar]
				if ok {
					newVars[param] = callingVarDetails
				}
			}

		}
		return newVars
	*/
}

// RefactorModels  in case of multiple run models, group the models and return an array per run model
func RefactorModels(ctx Context, g Grammar, m Match) []Match {
	matches := make([]Match, 0)

	i := 0
	for _, model := range g.Run {
		match := Match{}
		match.Match = make([]Match, 0)
		for j := i; j < len(m.Match); j++ {
			m := m.Match[j]
			if m.Model == model.Model {
				match.Match = append(match.Match, m)
			} else {
				i = j
				break
			}
		}
		newmatch := MergeMatches(match, ctx, Variable{})
		newmatch.Model = model.Model
		matches = append(matches, newmatch)
		ctx.Vars[model.Model] = newmatch
	}
	for index, meta := range g.Meta {
		logger.Debug("check meta", zap.Int("index", index), zap.String("meta", meta))
		compare, compareErr := CallCompareFunction(fmt.Sprintf("meta%d", index), ctx)
		if compareErr != nil || !compare {
			logger.Error("meta failed", zap.String("meta", meta), zap.Error(compareErr))
			return nil
		}

	}
	return matches
}

// ModelMetaControls checks for model meta
func ModelMetaControls(ctx Context, model string, match Match) (bool, error) {
	for index, meta := range ctx.Grammar.Models[model].Meta {
		logger.Debug("check meta", zap.Int("index", index), zap.String("meta", meta))
		newCtx := ctx
		newCtx.Vars = match.Vars
		funcName := fmt.Sprintf("%s_%s_meta%d", model, model, index)
		compare, compareErr := CallCompareFunction(funcName, newCtx)
		if compareErr != nil || !compare {
			logger.Debug("meta failed", zap.String("meta", meta), zap.Error(compareErr))
			return false, compareErr
		}
		//logger.Error("meta OK", zap.String("meta", meta), zap.Any("m", match))
	}
	return true, nil
}

// Res2Fasta reads result file (json) and generate results in Fasta format
func Res2Fasta(seqFile string, resFile string, outpath string) error {
	file, err := os.Open(resFile)
	if err != nil {
		return err
	}
	defer file.Close()

	seq, _ := os.Open(seqFile)
	defer seq.Close()

	var outFile *os.File
	var errFile error
	if outpath == "" {
		outFile, errFile = os.Create(resFile + ".fasta")
	} else {
		outFileName := path.Base(resFile) + ".fasta"
		outFile, errFile = os.Create(path.Join(outpath, outFileName))
	}

	if errFile != nil {
		logger.Error("Failed to create output file", zap.Error(errFile))
		return errFile
	}

	defer outFile.Close()

	scanner := bufio.NewScanner(file)
	index := 0
	for scanner.Scan() {
		line := scanner.Text()
		match := Match{}
		jsonErr := json.Unmarshal([]byte(line), &match)
		if jsonErr != nil {
			logger.Error("Failed to read match", zap.String("line", line))
			break
		}
		start := match.Position
		buffer := make([]byte, match.Len)
		seq.ReadAt(buffer, int64(start))
		// get content
		content := string(buffer)
		outFile.WriteString(fmt.Sprintf(">match %d\n", index))
		outFile.WriteString(content + "\n")
		index++
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

// CleanupFiles deletes a list of files
func CleanupFiles(files []FastaInfo) error {
	gotErr := false
	for _, f := range files {
		err := os.Remove(f.Path)
		if err != nil {
			gotErr = true
		}
	}
	if gotErr {
		return fmt.Errorf("failed to delete all files")
	}
	return nil
}

// Fasta2logol parse a fasta sequence file
// and generate 1 file per sequence (single line seq)
func Fasta2logol(seqpath string, outpath string) ([]FastaInfo, error) {

	outFiles := make([]FastaInfo, 0)
	_, err := os.Stat(seqpath)
	if os.IsNotExist(err) {
		return nil, err
	}

	file, err := os.Open(seqpath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	fileIndex := 0
	var outFile *os.File
	var errFile error
	scanner := bufio.NewScanner(file)
	header := ""
	uid := uuid.New()
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, ">") {
			if outFile != nil {
				outFile.Close()
				outFile = nil
			}
			header = line
			outFileName := fmt.Sprintf("%s.%d", path.Join(outpath, uid.String()), fileIndex)
			fastaInfo := FastaInfo{
				Sequence: seqpath,
				Path:     outFileName,
				Header:   header,
				Index:    fileIndex,
			}
			outFiles = append(outFiles, fastaInfo)
			outFile, errFile = os.Create(outFileName)
			if errFile != nil {
				return nil, errFile
			}
			fileIndex++
		} else {
			outFile.WriteString(strings.ToLower(line))
		}
	}
	if outFile != nil {
		outFile.Close()
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return outFiles, nil
}

// ZipFiles creates an archive with all input files
func ZipFiles(outDir string, files []string) (string, error) {
	uid := uuid.New()
	filename := path.Join(outDir, uid.String()+".logol.zip")
	newZipFile, err := os.Create(filename)
	if err != nil {
		return "", err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	// Add files to zip
	for _, file := range files {
		if err = addFileToZip(zipWriter, file); err != nil {
			return filename, err
		}
	}
	return filename, nil
}

func addFileToZip(zipWriter *zip.Writer, filename string) error {

	fileToZip, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fileToZip.Close()

	// Get the file information
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	// Using FileInfoHeader() above only uses the basename of the file. If we want
	// to preserve the folder structure we can overwrite this with the full path.
	header.Name = filename

	// Change to deflate to gain better compression
	// see http://golang.org/pkg/archive/zip/#pkg-constants
	header.Method = zip.Deflate

	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}
