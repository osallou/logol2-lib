package logol

import (
	"testing"
)

func TestUtilsFasta2Logol(t *testing.T) {
	files, err := Fasta2logol("testdata/test.fasta", "/tmp")
	if err != nil {
		t.Errorf("could not create files")
	}
	if len(files) == 0 {
		t.Errorf("should have a list of files")
	}
	err = CleanupFiles(files)
	if err != nil {
		t.Errorf("failed to delete files")
	}
}
